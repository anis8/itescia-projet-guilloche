import {App} from "../App.js";
import {HeaderComponent} from "./components/header/HeaderComponent.js";
import {DraggableComponent} from "./components/draggable/DraggableComponent.js";
import {SettingsComponent} from "./components/settings/SettingsComponent.js";
import {Network} from "../network/Network.js";
import {Detections} from "../objects/detection/Detections.js";

export class Interface {
    static init() {
        this.body = App.sn.el(document.getElementsByClassName("Body")[0]);
        this.header = new HeaderComponent(this);
        this.draggable = new DraggableComponent(this);
        this.settings = new SettingsComponent(this);

        let self = this;
        Network.request({
            type: "POST",
            pathname: "detections",
            success: async function (e) {
                Detections.detections = e;

                self.settings.render();
            }
        });


        this.header.render();
        this.draggable.render();
    }
}