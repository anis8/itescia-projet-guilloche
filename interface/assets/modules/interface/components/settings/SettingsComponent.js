import {InterfaceComponent} from "../../InterfaceComponent.js";
import {Detections} from "../../../objects/detection/Detections.js";

export class SettingsComponent extends InterfaceComponent {

    constructor(interfaces) {
        super();

        this.interfaces = interfaces;
        this.container = null;
    }


    render() {

        let options = '';
        for (let key in Detections.detections) {
            if (Detections.detections.hasOwnProperty(key)) {
                options += `<option value="${key}">${Detections.detections[key]}</option>`;
            }

        }
        //language=HTML
        let template = `
            <div class="settings">
                <div class="select-style pos-flex-globally">
                    <select class="Select txt-extra-bold">
                        ${options}
                    </select>
                </div>
            </div>
        `;

        this.container = this.interfaces.body.append(template);
    }

    getValue() {
        return this.container.get("Select").element.value;
    }


}