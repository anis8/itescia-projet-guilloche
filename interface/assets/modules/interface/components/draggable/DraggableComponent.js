import {InterfaceComponent} from "../../InterfaceComponent.js";
import {HttpFileUploader} from "../../../network/http/HttpFileUploader.js";
import {Network} from "../../../network/Network.js";

export class DraggableComponent extends InterfaceComponent {

    constructor(interfaces) {
        super();
        this.interfaces = interfaces;

        this.events = ['dragenter', 'dragover', 'dragleave', 'drop'];
        this.container = null;
        this.dragZone = null;
        this.dragImages = null;


        this.files = [];
        this.elements = [];
    }

    render() {
        let template = `
            <div class="draggable">
                <div class="DragIcon icon"></div>
                <div class="zone DragZone pos-dim-maxheight pos-dim-maxwidth pos-absolute"></div>
                <div class="images DragImages">
                    <div class="pos-float-end"></div>
                </div>
                <div style="position: absolute;bottom: -30px;left: calc(50% - 68px);" class="txt-bold">
                    Glisser & Déposer
                </div>
            </div>
        `;

        this.container = this.interfaces.body.append(template);
        this.dragZone = this.container.el("DragZone");
        this.dragImages = this.container.el("DragImages");

        let self = this;
        this.events.forEach(eventName => {
            self.container.addListener(eventName, self.preventDefaults);
            self.container.putListener(window, eventName, self.preventDefaults);
        });
        this.dragZone.addListener("dragenter", function () {
            self.container.addClass("drag");
        });
        this.dragZone.addListener("dragleave", function () {
            self.container.removeClass("drag");
        });
        this.dragZone.addListener("drop", function (e) {
            let dt = e.dataTransfer;
            let files = dt.files;
            if (files.length > 0) {
                self.container.addClass("border");
                self.container.get("DragIcon").hide();
                for (let i = 0; i < files.length; i++) {
                    if (HttpFileUploader.isImage(files[i].type)) {
                        self.addImage(files[i]);
                    }
                }
            }

            self.container.removeClass("drag");
        });

    }

    addImage(file) {
        let self = this;
        //language=HTML
        let image = self.dragImages.prepend(`
            <div class="image">
                <img class="Image" src=""/>
                <div style="height:100%;"
                     class="UploadLoader uploader-loader animate-spinner-circle-progress pos-absolute pos-dim-maxwidth pos-dim-maxheight pos-flex-globally">
                    <svg viewBox="0 0 114 114">
                        <circle class="Progress progress" transform="rotate(-90 57 57)" r="50" cx="57" cy="57"/>
                        <g class="Text text">
                            <text x="50%" y="50%" class="Number number">0%</text>
                        </g>
                    </svg>
                </div>
            </div>
        `);

        image.onclick(function () {
            image.remove();
            self.removeImage(file);
        });

        self.files.push(file);
        self.elements.push(file);

        self.upload(image, file);
    }

    removeImage(file) {
        let index = this.files.indexOf(file);
        if (index >= 0) {
            this.files.splice(index, 1);
            this.elements.splice(index, 1);
        }
        if (this.files.length === 0) {
            this.container.removeClass("border");
            this.container.get("DragIcon").show();

        }
    }

    preventDefaults(e) {
        e.preventDefault();
        e.stopPropagation();
    }

    circularProgress(element, v) {
        let progressCircle = element.get("Progress").element;
        let percentageText = element.get("Number").element;
        progressCircle.style.strokeDashoffset = (314.16 - ((v / 100) * 314.16));
        percentageText.textContent = v + "%";
    }

    upload(element, file) {
        let data = {
            typeName: this.interfaces.settings.getValue()
        };

        let self = this;
        let filesRequest = Network.fileRequest([file]);
        filesRequest.startUpload(data,
            function (e) {
                element.get("UploadLoader").remove();
                element.get("Image").addAttribute("src", Network.images + e[0].resultPath);

                let result = e[0].result;
                if (result) {
                    element.append(`<div style="background: #00d65c;" class="pos-absolute result txt-extra-bold">Vrai</div>`);
                } else {
                    element.append(`<div class="pos-absolute result txt-extra-bold">Faux</div>`);
                }
            }, function () {

            }, function (e) {
                self.circularProgress(element, e);
            });

    }

}