import {InterfaceComponent} from "../../InterfaceComponent.js";

export class HeaderComponent extends InterfaceComponent {

    constructor(interfaces) {
        super();

        this.interfaces = interfaces;
    }


    render() {
        //language=HTML

        let template = `
            <div class="header pos-flex-globally">
                <div class="icon"></div>
                <div class="title txt-extra-bold">
                    Fingerprint
                </div>
            </div>
        `;

        this.interfaces.body.append(template);
    }


}