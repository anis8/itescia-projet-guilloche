import {Snake} from "./snake/Snake.js";
import {SnakeMachine} from "./snake/utilities/SnakeMachine.js";
import {Objects} from "./objects/Objects.js";
import {Network} from "./network/Network.js";
import {Interface} from "./interface/Interface.js";

export class App {
    static init() {
        this.sn = Snake;
        this.sn.ready(async function () {
            await Promise.all([
                Snake.init(),
                SnakeMachine.init(),
                Objects.init(),
                Network.init(),
            ]);
        });

        Interface.init();
    }
}