import {Element} from "./elements/Element.js";
import {Editable} from "./elements/types/editable/Editable.js";
import {Ajax} from "./network/http/Ajax.js";
import {BytesSocket} from "./network/websocket/BytesSocket.js";
import {SnakeMachine} from "./utilities/SnakeMachine.js";
import {Table} from "./elements/types/table/Table.js";

export class Snake {

    static async init() {
        this.log = false;
        this.elements = [];

        if (this.log) {
            let self = this;
            SnakeMachine.setInterval(function () {
                // console.log(self.elements)
                console.log("elements: " + self.elements.length);
            }, 3000);
        }
    }

    static store(element) {
        if (this.log) {
            //console.log("element store")
        }
        this.elements.push(element);
    }

    static unStore(element) {
        if (this.log) {
            //console.log("element unstore")
        }
        let index = this.elements.indexOf(element);
        if (index >= 0) {
            this.elements.splice(index, 1);
        }
    }


    static ed(element) {
        return new Editable(element);
    }

    static el(element) {
        return new Element(element);
    }

    static tab(element) {
        return new Table(element);
    }

    static get(element) {
        return new Element(element, false);
    }

    static ready(callback) {
        if (document.readyState !== 'loading') callback();
        else if (document.addEventListener) document.addEventListener('DOMContentLoaded', callback);
        else document.attachEvent('onreadystatechange', function () {
                if (document.readyState === 'complete') callback();
            });
    }

    static scp(s) {
        return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
    }

    static http() {
        if (typeof this.httpServer === "undefined") {
            this.httpServer = new Ajax();
        }
        return this.httpServer;
    }

    static socket() {
        if (typeof this.socketServer === "undefined") {
            this.socketServer = new BytesSocket();
        }
        return this.socketServer;
    }

}