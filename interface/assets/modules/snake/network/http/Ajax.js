import {SnakeMachine} from "../../utilities/SnakeMachine.js";

export class Ajax {

    constructor() {
        this.port = null;
        this.host = null;
        this.settings = {};
    }

    //types: certificates,crossOrigin,dataType
    bind(host, port, settings) {
        this.port = port;
        this.host = host;
        this.settings = settings;
    }

    request(request) {
        request.url = this.host + (this.port === 0 ? "" : ":" + this.port) + "/" + request.pathname;
        this.sendRequest(request);
    }

    sendRequest(request) {
        let self = this;
        let xhr = new XMLHttpRequest();
        let error = false;

        xhr.open(request.type, request.url, true);
        xhr.withCredentials = true;

        let timeOut = SnakeMachine.setTimeout(function () {
            xhr.abort();
        }, 5000);


        xhr.onloadend = function (e) {
            clearTimeout(timeOut);

            if (xhr.status === 200) {
                if (typeof self.settings.dataType !== "undefined") {
                    if (self.settings.dataType === "json") {
                        request.success(JSON.parse(e.currentTarget.response));
                    }
                } else {
                    request.success(e);
                }
                if (typeof request.end !== "undefined") {
                    request.end(e);
                }
            } else {
                if (typeof request.error !== "undefined") {
                    if (!error) {
                        error = true;
                        request.error(e);
                    }
                }

                if (typeof request.end !== "undefined") {
                    request.end(e);
                }
            }
        };
        xhr.onerror = function (e) {
            clearTimeout(timeOut);
            if (typeof request.error !== "undefined") {
                if (!error) {
                    error = true;
                    request.error(e);
                }
            }
            if (typeof request.end !== "undefined") {
                request.end(e);
            }
        };

        if (typeof request.data !== "undefined") {
            let data = "";
            let counter = 1;
            Object.keys(request.data).forEach(function (k) {
                data += k + "=" + request.data[k] + (counter >= Object.keys(request.data).length ? "" : "&");
                counter++;
            });
            xhr.send(data);
        } else {
            xhr.send();
        }
    }

    file(request) {
        request.url = this.host + (this.port === 0 ? "" : ":" + this.port) + "/" + request.pathname;
        this.sendFile(request);
    }


    download(request) {
        let self = this;
        let xhr = new XMLHttpRequest();

        let error = false;

        let data = new FormData();
        if (typeof request.data !== "undefined") {
            Object.keys(request.data).forEach(function (k) {
                data.append(k, request.data[k]);
            });
        }


        xhr.open(request.type, request.url, true);
        xhr.responseType = 'blob';
        xhr.withCredentials = true;

        let timeOut = SnakeMachine.setTimeout(function () {
            xhr.abort();
        }, 60000);

        xhr.onprogress = function (evt) {
            if (typeof request.progress !== "undefined") {
                if (evt.lengthComputable) {
                    request.progress(parseInt((evt.loaded / evt.total) * 100));
                }
            }
        };

        xhr.onloadend = function (e) {
            clearTimeout(timeOut);

            if (xhr.status === 200) {
                if (typeof self.settings.dataType !== "undefined") {
                    if (self.settings.dataType === "json") {
                        request.success(e.currentTarget.response);
                    }
                } else {
                    request.success(e);
                }
                if (typeof request.end !== "undefined") {
                    request.end(e);
                }
            } else {
                if (typeof request.error !== "undefined") {
                    if (!error) {
                        error = true;
                        request.error(e);
                    }
                }

                if (typeof request.end !== "undefined") {
                    request.end(e);
                }
            }
        };
        xhr.onerror = function (e) {
            clearTimeout(timeOut);
            if (typeof request.error !== "undefined") {
                if (!error) {
                    error = true;
                    request.error(e);
                }
            }
            if (typeof request.end !== "undefined") {
                request.end(e);
            }
        };
        xhr.send(data);

        return xhr;

    }

    sendFile(request) {
        let self = this;
        let xhr = new XMLHttpRequest();

        let error = false;

        let data = new FormData();
        data.append('file', request.file);
        if (typeof request.data !== "undefined") {
            Object.keys(request.data).forEach(function (k) {
                data.append(k, request.data[k]);
            });
        }


        xhr.open(request.type, request.url, true);
        xhr.withCredentials = true;

        let timeOut = SnakeMachine.setTimeout(function () {
            xhr.abort();
        }, 60000);

        xhr.upload.onprogress = function (evt) {
            if (typeof request.progress !== "undefined") {
                if (evt.lengthComputable) {
                    request.progress(parseInt((evt.loaded / evt.total) * 100));
                }
            }
        };

        xhr.onloadend = function (e) {
            clearTimeout(timeOut);
            if (xhr.status === 200) {
                if (typeof self.settings.dataType !== "undefined") {
                    if (self.settings.dataType === "json") {
                        request.success(JSON.parse(e.currentTarget.response));
                    }
                } else {
                    request.success(e);
                }
                if (typeof request.end !== "undefined") {
                    request.end(e);
                }
            } else {
                if (typeof request.error !== "undefined") {
                    if (!error) {
                        error = true;
                        request.error(e);
                    }
                }

                if (typeof request.end !== "undefined") {
                    request.end(e);
                }
            }
        };
        xhr.onerror = function (e) {
            clearTimeout(timeOut);
            if (typeof request.error !== "undefined") {
                if (!error) {
                    error = true;
                    request.error(e);
                }
            }
            if (typeof request.end !== "undefined") {
                request.end(e);
            }
        };
        xhr.send(data);

    }
}