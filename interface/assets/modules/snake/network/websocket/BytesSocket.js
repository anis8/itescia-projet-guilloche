import {OutgoingBuffer} from "./buffers/OutgoingBuffer.js";
import {IncomingBuffer} from "./buffers/IncomingBuffer.js";
import {SnakeMachine} from "../../utilities/SnakeMachine.js";

export class BytesSocket {
    bind(method, host, port, handler) {
        let self = this;
        this.host = host;
        this.port = port;
        this.method = method;
        this.connection = new WebSocket(this.method + "://" + this.host + ":" + this.port + "/");
        this.connection.binaryType = "arraybuffer";

        this.timeOut = SnakeMachine.setTimeout(function () {
            console.log("close")
            self.connection.close();
        }, 5000);


        this.connection.onmessage = function (e) {
            handler(e);
        };


        this.connection.onopen = function () {
            clearTimeout(self.timeOut);
        }
    };

    write(arrayMessage) {
        this.connection.send(arrayMessage.buffer);
    }

    ready(func) {
        let self = this;
        this.connection.onopen = function () {
            clearTimeout(self.timeOut);
            func();
        }
    }

    error(func) {
        this.connection.onerror = function (e) {
            func(e);
        };
    }

    close(func) {
        this.connection.onclose = function (e) {
            func(e);
        }
    }

    newMessage() {
        return new OutgoingBuffer();
    }

    readMessage(e) {
        return new IncomingBuffer(e.data);
    }
}