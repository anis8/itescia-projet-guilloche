import {IncomingBuffer} from "../buffers/IncomingBuffer.js";

export class MessageHandler {
    constructor(msg) {
        this.message = new IncomingBuffer(msg.data);
    }
}