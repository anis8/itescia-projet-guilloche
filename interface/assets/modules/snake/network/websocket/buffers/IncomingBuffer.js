export class IncomingBuffer {

    constructor(buffer) {
        this.buffer = null;
        this.bufferSlot = 0;
        this.view = null;


        this.buffer = buffer;
        this.view = new DataView(buffer);
        let size = this.view.getInt32(this.bufferSlot) + 4;
        this.bufferSlot = this.bufferSlot + 4;
        if (size > this.buffer.length) {
            console.log("SNK WS: incoming packet: Incomplete.");
        }
    }

    clear() {
        this.buffer = null;
        this.bufferSlot = 0;
        this.view = null;
    }

    readInt() {
        let result = this.view.getInt32(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 4;
        return result;
    }

    readChar() {
        let result = this.view.getInt16(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 2;
        return String.fromCharCode(result);
    }

    readBoolean() {
        let result = this.view.getInt8(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 1;
        return !!result;
    }

    readShort() {
        let result = this.view.getInt16(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 2;
        return result;
    }

    readByte() {
        let result = this.view.getInt8(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 1;
        return result;
    }

    readFloat() {
        let result = this.view.getFloat32(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 4;
        return result;
    }

    readLong() {
        let result = this.view.getBigInt64(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 8;
        return BigInt(result);
    }

    readDouble() {
        let result = this.view.getFloat64(this.bufferSlot);
        this.bufferSlot = this.bufferSlot + 8;
        return result;
    }

    readString() {
        let size = (this.view.getInt16(this.bufferSlot));
        this.bufferSlot = this.bufferSlot + 2;
        let arrayString = [];
        for (let i = 0; i < size; i++) {
            arrayString.push(this.view.getUint8(this.bufferSlot));
            this.bufferSlot++;
        }
        let str = '';
        for (let i = 0; i < arrayString.length; i++) {
            let value = arrayString[i];

            if (value < 0x80) {
                str += String.fromCharCode(value);
            } else if (value > 0xBF && value < 0xE0) {
                str += String.fromCharCode((value & 0x1F) << 6 | arrayString[i + 1] & 0x3F);
                i += 1;
            } else if (value > 0xDF && value < 0xF0) {
                str += String.fromCharCode((value & 0x0F) << 12 | (arrayString[i + 1] & 0x3F) << 6 | arrayString[i + 2] & 0x3F);
                i += 2;
            } else {
                let charCode = ((value & 0x07) << 18 | (arrayString[i + 1] & 0x3F) << 12 | (arrayString[i + 2] & 0x3F) << 6 | arrayString[i + 3] & 0x3F) - 0x010000;
                str += String.fromCharCode(charCode >> 10 | 0xD800, charCode & 0x03FF | 0xDC00);
                i += 3;
            }
        }

        return str;
    }
}