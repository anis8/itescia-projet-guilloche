export class SnakeUtils {

    static arraySortNumberAsc(object, key) {
        return object.sort((a, b) => a[key] - b[key]);
    }

    static arraySortNumberDesc(object, key) {
        return this.arraySortNumberAsc(object, key).reverse();
    }

    static arraySortTextAsc(object, key) {
        object.sort(function (a, b) {
            if (a[key] === b[key])
                return 0;

            return ((a[key] < b[key])) ? 1 : -1;
        });
    }

    static arraySortTextDesc(object, key) {
        object.sort(function (a, b) {
            if (a[key] === b[key])
                return 0;

            return ((a[key] < b[key])) ? -1 : 1;
        });
    }

    static arrayMove(arr, oldIndex, newIndex) {
        let newArray = arr.slice(0);
        if (newIndex >= newArray.length) {
            let k = newIndex - newArray.length + 1;
            while (k--) {
                newArray.push(undefined);
            }
        }
        newArray.splice(newIndex, 0, newArray.splice(oldIndex, 1)[0]);
        return newArray;
    }

    static randomInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    static randomString(length) {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    static txtIsEmoji(str) {
        let ranges = [
            '(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32-\ude3a]|[\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])' // U+1F680 to U+1F6FF
        ];
        return !!str.match(ranges.join('|'));
    }

    static txtCountEmoji(str) {
        const joiner = "\u{200D}";
        const split = str.split(joiner);
        let count = 0;

        for (const s of split) {
            const num = Array.from(s.split(/[\ufe00-\ufe0f]/).join("")).length;
            count += num;
        }
        return count / split.length;
    }

    static txtUrilify(string) {
        let urlRegex = /(((https?:\/\/)|(www\.))[^\s]+)/g;
        return string.replace(urlRegex, function (url, b, c) {
            let url2 = (c === 'www.') ? 'http://' + url : url;
            let fetch = "";
            if (url2.substr(0, 8) === "https://") {
                fetch = url2.substr(8, url2.length);
            } else if (url2.substr(0, 7) === "http://") {
                fetch = url2.substr(7, url2.length);
            } else if (url2.substr(0, 4) === "www.") {
                fetch = url2.substr(4, url2.length);
            } else {
                fetch = url2;
            }
            return '<a href="' + url2 + '" target="_blank">' + fetch + '</a>';
        });
    }

    static txtCapitalize(value) {
        return value.charAt(0).toUpperCase() + value.slice(1);
    }

    static timeZone(timeZone) {
        return timeZone;
    }

    static time() {
        let time = Date.now();
        return parseInt(time / 1000);
    }

}