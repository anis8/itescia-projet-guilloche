export class SnakeMachine {

    static async init() {
        this.listeners = [];
        this.log = false;

        if (this.log) {
            let self = this;
            this.setInterval(function () {
                console.log("listeners " + self.listeners.length);
            }, 3000);
        }
    }

    static setTimeout(func, time) {
        return setTimeout(function () {
            func();
        }, time);
    }

    static setInterval(func, time) {
        //let c = Utilities.randomString(5);
        return setInterval(function () {
            func();
        }, time);
    }

    static removeListener(data) {
        if (data == null) {
            return;
        }
        if (this.log) {
            //console.log("remove listener: " + data[1]);
        }
        data[0].removeEventListener(data[1], data[2]);
        let index = this.listeners.indexOf(data);
        if (index >= 0) {
            this.listeners.splice(index, 1);
        }
    }

    static addListener(element, name, callback) {
        if (this.log) {
            //console.log("add listener: " + name);
        }

        element.addEventListener(name, callback);
        let listener = [element, name, callback];
        this.listeners.push(listener);
        return listener;
    }

    static onReady(condition, callback, timeOut, timer, tick) {
        let time = this.setTimeout(function () {
            timeOut();
            clearTimeout(time);
            clearInterval(interval);
        }, typeof timer === "undefined" ? 7000 : timer);
        let interval = this.setInterval(function () {
            if (condition()) {
                callback();
                clearTimeout(time);
                clearInterval(interval);
            }
        }, typeof tick === "undefined" ? 50 : tick);
    }

    static randomInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}