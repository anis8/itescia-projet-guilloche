import {Element} from "../../Element.js";

export class TextBox extends Element {
    constructor(element) {
        super(element);
        this.caretPosition = 0;
        this.write = true;
        this.ctrlPressed = false;
    };

    pasteText() {
        this.addListener("paste", function (e) {
            e.preventDefault();
            var text = (e.originalEvent || e).clipboardData.getData('text/plain');
            document.execCommand("insertHTML", false, text);
        });
    }

    setWrite(write) {
        this.write = write;
    }

    canWrite() {
        return this.write;
    }

    setHtml(html) {
        this.getCursorPosition();
        this.element.innerHTML = html;
        this.setCursorPosition(this.element, this.caretPosition);
    };

    initHtml(html) {
        this.element.innerHTML = html;
    }

    handleKeys() {
        let self = this;
        this.keysHandled = {};
        let keyup = function (e) {
            self.keysHandled[e.code] = false;
        };
        this.addListener('keyup', keyup);
        let keydown = function (e) {
            self.keysHandled[e.code] = true;
        };
        this.addListener('keydown', keydown);
    };

    setLimit(limit) {
        //TODO rajouter pour le copier coller
        let self = this;
        this.addListener('keydown', function (e) {
            if (e.which === 17) {
                self.ctrlPressed = true;
            }
        });

        this.addListener('keyup', function (e) {
            if (e.which === 17) {
                self.ctrlPressed = false;
            }
        });
        this.addListener('keydown', function (e) {
            if (e.which <= 90 && e.which >= 48) {
                if (self.getHtml().length >= limit && !self.ctrlPressed) {
                    e.preventDefault();
                }
            }
        });
    }

    onKeyEvent(func, delay) {
        this.addListener('keydown', this.delay(func, (typeof delay === "undefined" ? 50 : delay)));
    };

    onEnterKeyEvent(func, delay) {
        func = this.delay(func, (typeof delay === "undefined" ? 50 : delay));
        let globalFunc = function (e) {
            if (e.code === "Enter" || e.code === "NumpadEnter") {
                e.preventDefault();
                if (func !== null) {
                    func();
                }
            }
        };
        this.addListener('keypress', globalFunc);
    };

    onPasteEvent(func, delay) {
        this.addListener('paste', this.delay(func, (typeof delay === "undefined" ? 50 : delay)));
    };

    setCursorAtEnd() {
        let range, selection;
        if (document.createRange) {
            range = document.createRange();
            range.selectNodeContents(this.element);
            range.collapse(false);
            selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
        }
    };

    setCursorPosition(el, pos) {
        for (let node of el.childNodes) {
            if (node.nodeType === 3) {
                if (node.length >= pos) {
                    if (pos !== -1) {
                        let range = document.createRange(),
                            sel = window.getSelection();
                        range.setStart(node, pos);
                        range.collapse(true);
                        sel.removeAllRanges();
                        sel.addRange(range);
                    }
                    return -1;
                } else {
                    pos -= node.length;
                }
            } else {
                pos = this.setCursorPosition(node, pos);
                if (pos === -1) {
                    return -1;
                }
            }
        }
        return pos;
    };

    place(html) {
        var sel, range;
        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);
                range.deleteContents();
                let basic = range.cloneRange();
                if (basic.endContainer === this.element ||
                    basic.endContainer.parentNode === this.element
                ) {
                    var el = document.createElement("div");
                    el.innerHTML = html;
                    var frag = document.createDocumentFragment(), node, lastNode;
                    while ((node = el.firstChild)) {
                        lastNode = frag.appendChild(node);

                        let nodeEm = lastNode;
                        lastNode.onclick = function () {
                            if (nodeEm.parentNode !== null) {
                                nodeEm.parentNode.removeChild(nodeEm);
                            }
                        }
                    }
                    range.insertNode(frag);

                    // Preserve the selection
                    if (lastNode) {
                        range = range.cloneRange();
                        range.setStartAfter(lastNode);
                        range.collapse(true);
                        sel.removeAllRanges();
                        sel.addRange(range);
                    }
                }
            }
        }

    }

    getCursorPosition() {
        let element = this.element;
        let isChildOf = function (node, parentId) {
            while (node !== null) {
                if (node.id === parentId) {
                    return true;
                }
                node = node.parentNode;
            }
            return false;
        };
        let selection = window.getSelection(),
            charCount = -1,
            node;

        if (selection.focusNode) {
            if (isChildOf(selection.focusNode, element.id)) {
                node = selection.focusNode;
                charCount = selection.focusOffset;

                while (node) {
                    if (node.id === element.id) {
                        break;
                    }

                    if (node.previousSibling) {
                        node = node.previousSibling;
                        charCount += node.textContent.length;
                    } else {
                        node = node.parentNode;
                        if (node === null) {
                            break
                        }
                    }
                }
            }
        }
        this.caretPosition = charCount;
        return charCount;
    };

    urlify() {
        this.handleKeys();
        let self = this;
        let urlRegex = /(((https?:\/\/)|(www\.))[^\s]+)/g;
        let transform = function (text) {
            return text.replace(urlRegex, function (url, b, c) {
                let url2 = (c === 'www.') ? 'https://' + url : url;
                return '<a href="' + url2 + '" target="_blank">' + url + '</a>';
            });
        };
        self.onKeyEvent(function () {
            if (typeof self.keysHandled['ControlLeft'] === "undefined" || (typeof self.keysHandled['ControlLeft'] !== "undefined" && self.keysHandled['ControlLeft'] === false)) {
                self.setHtml(transform(self.getText()));
            }
        }, 10);
    };

    displayCaret() {
        this.element.style.display = "inline-block";
    }

    focus() {
        this.element.focus();
    }

    basicPaste() {
        this.addListener("paste", function (e) {
            e.preventDefault();
            let text = (e.originalEvent || e).clipboardData.getData('text/plain');
            document.execCommand("insertHTML", false, text);
        });
    }
}