export class TableHeaderColumnsFilter {
    constructor(header) {
        this.header = header;

        this.container = this.header.container.el("ColumnsFilterBloc");
        this.button = this.container.el("Button");

        //language=HTML
        let template = `
            <div class="Menu menu box-white-radius-2-shadow-3 pos-absolute"></div>
        `;

        this.container.append(template);

        this.renderColumns();


        this.button.onclick(function () {
            console.log("ddd")
        })
    }


    renderColumns() {
        let self = this;
        for (let i = 0; i < this.header.table.data.columns.length; i++) {
            let column = this.header.table.data.columns[i];
            //language=HTML
            let template = `
                <div class="column pos-relative">
                    <label class="pos-relative btn-checkbox pos-float-left pos-mrg-right-1">
                        <input class="Checkbox" ${column.visible ? "checked" : ""} type="checkbox"/>
                        <span class="eff-cursor-outline-hover-3 checkmark"></span>
                    </label>
                    <div class="name pos-relative txt-bold pos-float-left">
                        ${column.name}
                    </div>
                    <div class="icon pos-absolute table-ico-drag"></div>
                    <div class="pos-float-end"></div>
                </div>
            `;

            let menu = this.container.el("Menu").append(template);
            let checkbox = menu.el("Checkbox");
            checkbox.onclick(function () {
                let checked = checkbox.element.checked;
                self.header.table.container.updateColumnVisibility(column.id, checked);
                self.header.table.container.clear();
                self.header.table.container.init();
            });
        }


    }

    renderColumn() {

    }

    remove(){
        this.container.remove();
        this.container = null;
        this.button = null;
    }
}