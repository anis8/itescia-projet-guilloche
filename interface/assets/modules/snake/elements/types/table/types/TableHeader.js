import {TableHeaderColumnsFilter} from "./header/TableHeaderColumnsFilter.js";

export class TableHeader {
    constructor(table) {
        this.table = table;
        //language=HTML
        let template = `
            <div class="header pos-relative pos-dim-maxwidth">
                <div class="left-block pos-relative pos-float-left">
                    <div class="icon pos-absolute table-ico-search"></div>
                    <input spellcheck="false" class="inp-white" placeholder="Rechercher dans la liste"/>
                </div>
                <div class="right-block pos-float-right pos-dim-maxheight">
                    <div style="margin-left:9px;" class="bloc pos-float-left pos-dim-maxheight pos-flex-globally">
                        <div class="button btn-round pos-flex-globally eff-cursor-alt-bottom">
                            <div style="height: 15px;width: 15px;" class="icon table-ico-export"></div>
                            <div class="alt-title">
                                Exporter
                            </div>
                        </div>
                    </div>
                    <div style="margin-left:10px;width:110px;"
                         class="bloc pos-float-left pos-dim-maxheight pos-flex-globally">
                        <div class="button big-button pos-flex-globally btn-round">
                            <div class="pos-float-left icon table-ico-filter"></div>
                            <div class="text pos-float-left txt-bold">Filtres</div>
                            <div class="number txt-bold pos-flex-globally">
                                3
                            </div>
                        </div>
                    </div>
                    <div class="ColumnsFilterBloc bloc pos-float-left pos-dim-maxheight pos-flex-globally">
                        <div class="Button button btn-round pos-flex-globally eff-cursor-alt-bottom">
                            <div class="icon table-ico-columns"></div>
                            <div class="alt-title">
                                Colonnes
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;

        this.container = this.table.append(template);
        this.columnsFilter = new TableHeaderColumnsFilter(this);
    }

    remove() {
        this.container = null;
        this.columnsFilter.remove();
        this.columnsFilter = null;
    }
}