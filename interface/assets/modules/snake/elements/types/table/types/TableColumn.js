import {SnakeMachine} from "../../../../utilities/SnakeMachine.js";
import {SnakeUtils} from "../../../../utilities/SnakeUtils.js";

export class TableColumn {
    constructor(tableContainer, column) {
        this.tableContainer = tableContainer;
        this.column = column;
        this.container = null;
        this.entriesContainer = null;
        this.loadingContainer = null;
        this.width = 0;
        this.visible = column.visible;

        this.isSortable = true;
        this.isDraggable = true;
        this.aproximateSize = 5;

        this.mouseDownListener = null;
        this.mouseUpListener = null;
        this.mouseMoveListener = null;
        this.focusOutListener = null;

        this.render();
    }


    render() {
        //language=HTML
        let template = `
            <div class="Column column pos-relative pos-float-left">
                <div class="Title title txt-extra-bold pos-flex-vertically">
                    <div class="SlidingCursor sliding-cursor ${this.isDraggable ? "eff-cursor-type-grabbing" : ""} pos-absolute"></div>
                    <div class="TitleBlock ${this.isSortable ? "btn-round" : ""} block">
                        <div class="pos-relative">${this.column.name}</div>
                        <div class="Icon icon pos-absolute table-ico-order-asc"></div>
                    </div>
                </div>
                <div class="Entries entries"></div>
            </div>
        `;

        this.container = this.tableContainer.container.append(template);
        this.entriesContainer = this.container.el("Entries");

        this.sortable();
        this.draggable();
    }

    renderLoading() {
        let template = ``;
        for (let i = 0; i < this.aproximateSize; i++) {
            template += `
                <div class="entry txt-semi-bold pos-flex-vertically">
                   <div style="height: 10px;width:${SnakeUtils.randomInteger(20, 60)}%;border-radius: 20px;background: #e6ebeb;" class="pos-relative"></div>
                </div>
            `;
        }

        this.loadingContainer = this.container.get("Entries").append('<div>' + template + '</div>');
        this.container.get("Entries").addClass("animate-shine-basic");
    }

    removeLoading() {
        if (this.loadingContainer != null) {
            this.loadingContainer.remove();
            this.loadingContainer = null;
            this.container.get("Entries").removeClass("animate-shine-basic");
        }
    }

    renderEntries() {
        this.clearEntries();
        let entries = '';
        switch (this.column.type) {
            case "text":
            case "number":
                entries = this.generateTextEntries(this.column);
                break;
            case "rating":
                entries = this.generateRatingEntries(this.column);
                break;
            case "progress":
                entries = this.generateProgressionEntries(this.column);
                break;
            case "color":
                entries = this.generateColorEntries(this.column);
                break;
        }

        return this.entriesContainer.append('<div>' + entries + '</div>');
    }

    clearEntries() {
        this.entriesContainer.clear();
    }

    generateTextEntries() {
        let template = ``;
        for (let i = 0; i < this.tableContainer.table.data.entries.length; i++) {
            let entry = this.tableContainer.table.data.entries[i];
            template += `
                <div class="entry txt-semi-bold pos-flex-vertically">
                    ${entry[this.column.id]}
                </div>
            `;
        }
        return template;
    }

    generateRatingEntries() {
        let template = ``;
        for (let i = 0; i < this.tableContainer.table.data.entries.length; i++) {
            let value = this.tableContainer.table.data.entries[i][this.column.id];
            let valueTemplate = ``;
            for (let s = 0; s < 5; s++) {
                valueTemplate += `<span ${s < value ? 'class="rate"' : ''}>.</span>`;
            }

            console.log(value)
            template += `
                <div class="entry txt-semi-bold pos-flex-vertically">
                    ${valueTemplate}
                </div>
            `;
        }
        return template;
    }

    generateProgressionEntries() {
        let template = ``;
        for (let i = 0; i < this.tableContainer.table.data.entries.length; i++) {
            let value = this.tableContainer.table.data.entries[i][this.column.id];
            template += `
                <div class="entry txt-semi-bold pos-flex-vertically">
                    <div class="progression pos-relative">
                        <div style="width:${value}%;" class="progress pos-relative pos-dim-maxheight"></div>
                    </div>
                </div>
            `;
        }
        return template;
    }

    generateColorEntries() {
        let template = ``;
        for (let i = 0; i < this.tableContainer.table.data.entries.length; i++) {
            let value = this.tableContainer.table.data.entries[i][this.column.id];
            template += `
                <div class="entry txt-semi-bold pos-flex-vertically">
                    <div style="background:${value};height: 15px;width: 15px;border-radius: 100%;" class="pos-relative"></div>
                </div>
            `;
        }
        return template;
    }

    getWidth() {
        return this.width;
    }

    resize() {
        let width = this.column.width;
        if (width.includes("%")) {
            let containerWidth = this.tableContainer.table.getWidth();
            width = width.replace("%", "");
            width = ((width * containerWidth)) / 100;
            this.width = width;
            this.container.get("Column").setStyle("width:" + width + "px");
        } else {
            this.width = parseInt(width);
            this.container.get("Column").setStyle("width:" + width);
        }
    }

    addWidth(width) {
        this.width += width;
        this.container.get("Column").setStyle("width:" + this.width + "px");
    }

    sortable() {
        if (!this.isSortable) {
            return;
        }
        let self = this;
        let titleBlock = self.container.el("TitleBlock");
        titleBlock.onclick(function () {
            self.tableContainer.removeColumnsOrder(self.column.id);
            let type = "desc";
            if (self.column.orderBy === "desc") {
                type = "asc";
                titleBlock.get("Icon").setStyle("opacity:1;transform: rotate(180deg);");
            } else {
                titleBlock.get("Icon").setStyle("opacity:1;transform: rotate(0deg);");
            }
            self.tableContainer.updateColumnOrder(self.column.id, type);
        });
    }

    draggable() {
        if (!this.isDraggable) {
            return;
        }

        //TODO mettre en el et faire passer les events par sa class
        let element = this.container.get("SlidingCursor");
        let table = this.tableContainer.table;

        let self = this;

        this.mouseDownListener = null;
        this.mouseUpListener = null;
        this.mouseMoveListener = null;
        this.focusOutListener = null;

        let tableWidth = 0;
        let containerWidth = 0;
        let mousePressX = 0;
        let mousePressWidth = 0;
        let isDown = false;

        let mouseUp = function () {
            isDown = false;
            mousePressX = 0;
            mousePressWidth = 0;

            SnakeMachine.removeListener(self.mouseUpListener);
            SnakeMachine.removeListener(self.focusOutListener);
            SnakeMachine.removeListener(self.mouseMoveListener);
            self.mouseUpListener = null;
            self.mouseMoveListener = null;
            self.focusOutListener = null;
        };
        let mouseDown = function (e) {
            isDown = true;
            self.mouseUpListener = SnakeMachine.addListener(element.element, 'mouseup', mouseUp);
            self.focusOutListener = SnakeMachine.addListener(element.element, 'mouseleave', mouseUp);
            self.mouseMoveListener = SnakeMachine.addListener(element.element, 'mousemove', mouseMove);
            containerWidth = self.tableContainer.container.getWidth();
            tableWidth = table.getWidth();
            mousePressX = e.pageX;
            mousePressWidth = (self.tableContainer.container.getStyle("left") === "" ? 0 : parseInt(self.tableContainer.container.getStyle("left")));
        };
        let mouseMove = function (e) {
            if (isDown) {
                let calc = e.pageX - mousePressX;
                let left = self.tableContainer.container.getStyle("left") === "" ? 0 : parseInt(self.tableContainer.container.getStyle("left"));


                self.tableContainer.slidingLeftButton.show();
                self.tableContainer.slidingRightButton.show();
                if (calc > 0) {
                    if (left >= 0) {
                        self.tableContainer.container.setStyle("left: 0px");
                        self.tableContainer.slidingLeftButton.hide();
                        return;
                    }
                }
                if (calc < 0) {
                    if ((Math.abs(left) + tableWidth) >= containerWidth) {
                        self.tableContainer.container.setStyle("width: " + containerWidth + "px");
                        self.tableContainer.slidingRightButton.hide();
                        return;
                    }
                }
                self.tableContainer.container.setStyle("left:" + (mousePressWidth + calc) + "px");
            }
        };
        self.mouseDownListener = SnakeMachine.addListener(element.element, 'mousedown', mouseDown);
    }

    isVisible() {
        return this.visible;
    }

    removeIcon() {
        this.container.get("TitleBlock").get("Icon").setStyle("opacity:0;transform: rotate(90deg);");
    }

    remove() {
        this.removeLoading();
        this.entriesContainer.remove();
        SnakeMachine.removeListener(this.mouseUpListener);
        SnakeMachine.removeListener(this.focusOutListener);
        SnakeMachine.removeListener(this.mouseMoveListener);
        SnakeMachine.removeListener(this.mouseDownListener);
    }
}