import {TableColumn} from "./TableColumn.js";
import {SnakeMachine} from "../../../../utilities/SnakeMachine.js";
import {SnakeUtils} from "../../../../utilities/SnakeUtils.js";

export class TableContainer {
    constructor(table) {
        this.table = table;
        //language=HTML
        let template = `
            <div class="container"></div>
        `;

        this.container = this.table.append(template);

        this.slidingRightButton = null;
        this.slidingLeftButton = null;

        this.columns = [];

        this.init();
    };


    init() {
        let self = this;
        let columns = this.generateColumnsList(this.table.data.columns);
        for (let i = 0; i < columns.length; i++) {
            this.columns.push(new TableColumn(this, columns[i]));
        }
        this.renderLoading();
        SnakeMachine.setTimeout(function () {
            self.removeLoading();
            self.renderEntries();
        }, 1000);

        this.resize();
        this.resizable();
    }

    renderEntries() {
        for (let i = 0; i < this.columns.length; i++) {
            this.columns[i].renderEntries();
        }
    }

    clearEntries() {
        for (let i = 0; i < this.columns.length; i++) {
            this.columns[i].clearEntries();
        }
    }

    clear() {
        for (let i = 0; i < this.columns.length; i++) {
            this.columns[i].remove();
        }
        this.columns = [];
        this.container.clear();
    }

    renderLoading() {
        for (let i = 0; i < this.columns.length; i++) {
            this.columns[i].renderLoading();
        }
    }

    removeLoading() {
        for (let i = 0; i < this.columns.length; i++) {
            this.columns[i].removeLoading();
        }
    }

    renderSliding() {
        //language=HTML
        let slidingContainer = this.table.append(`
            <div class="sliding">
                <div class="SlidingRight right pos-absolute eff-cursor-outline-hover-3 pos-flex-globally">
                    <div class="icon table-ico-next"></div>
                </div>
                <div class="SlidingLeft left pos-absolute eff-cursor-outline-hover-3 pos-flex-globally">
                    <div class="icon table-ico-back"></div>
                </div>
            </div>
        `);


        this.slidingRightButton = slidingContainer.el("SlidingRight");
        this.slidingLeftButton = slidingContainer.el("SlidingLeft");

        let distance = 200;
        let speed = 150;

        let self = this;
        self.slidingRightButton.onclick(function () {
            let container = self.container;
            let containerWidth = container.getWidth();
            let tableWidth = self.table.getWidth();
            let offsetLeft = Math.abs(container.getParentOffsets().left);

            if ((offsetLeft + tableWidth) < containerWidth) {
                self.slidingLeftButton.show();
                let calc = containerWidth - (offsetLeft + tableWidth);
                let val = distance;
                if (calc <= distance) {
                    val = calc;
                    self.slidingRightButton.hide();
                }
                self.container.animateLeft(-val, speed);
            }
        });

        self.slidingLeftButton.onclick(function () {
            let container = self.container;
            let offsetLeft = Math.abs(container.getParentOffsets().left);

            if (offsetLeft > 0) {
                self.slidingRightButton.show();
                let val = distance;
                if (offsetLeft <= distance) {
                    self.slidingLeftButton.hide();
                    val = offsetLeft;
                }
                self.container.animateLeft(val, speed);
            }
        });

    }

    hideSliding() {
        this.slidingRightButton.show();
        this.slidingLeftButton.show();
        if (this.slidingRightButton != null && this.slidingLeftButton != null) {
            let offsetLeft = this.container.getParentOffsets().left;
            if (offsetLeft === 0) {
                this.slidingLeftButton.hide();
            }
            let containerWidth = this.container.getWidth();
            let tableWidth = this.table.getWidth();
            if ((offsetLeft + tableWidth) >= containerWidth) {
                this.slidingRightButton.hide();
            }
        }
    }

    removeColumnsOrder(currentColumnId) {
        for (let i = 0; i < this.table.data.columns.length; i++) {
            let column = this.table.data.columns[i];
            if (column.id !== currentColumnId) {
                this.table.data.columns[i].orderBy = null;
            }
        }
        for (let i = 0; i < this.columns.length; i++) {
            this.columns[i].removeIcon();
        }
    }

    updateColumnOrder(id, type) {
        for (let i = 0; i < this.table.data.columns.length; i++) {
            let column = this.table.data.columns[i];
            if (column.id === id) {
                this.sortEntries(column, type);
                column.orderBy = type;
            }
        }
    }

    updateColumnVisibility(id, type) {
        for (let i = 0; i < this.table.data.columns.length; i++) {
            let column = this.table.data.columns[i];
            if (column.id === id) {
                column.visible = type;
            }
        }
    }

    sortEntries(column, type) {
        switch (column.type) {
            case "text":
            case "color":
                if (type === "asc") {
                    SnakeUtils.arraySortTextAsc(this.table.data.entries, column.id);
                } else {
                    SnakeUtils.arraySortTextDesc(this.table.data.entries, column.id);
                }
                break;
            case "number":
            case "rating":
            case "progress":
                if (type === "asc") {
                    SnakeUtils.arraySortNumberAsc(this.table.data.entries, column.id);
                } else {
                    SnakeUtils.arraySortNumberDesc(this.table.data.entries, column.id);
                }
                break;
        }
        this.clearEntries();
        this.renderEntries();
    }

    generateColumnsList(columns) {
        columns.sort((a, b) => a.order - b.order);
        let list = [];
        for (let i = 0; i < columns.length; i++) {
            let column = columns[i];
            if (column.visible) {
                list.push(column);
            }
        }
        return list;
    }

    resizable() {
        let self = this;
        this.table.onResize(function () {
            self.resize();
        })
    }

    resize() {
        this.resizeColumns();
        this.resizeContainer();
    }

    resizeContainer() {
        let columnsWidth = this.getColumnsWidth();
        this.container.setStyle("width:" + (Math.ceil(columnsWidth)) + "px");
        if ((columnsWidth - 1) > this.table.getWidth()) {
            if (this.slidingLeftButton == null && this.slidingRightButton == null) {
                this.renderSliding();
            }
        }

        if (this.table.getWidth() > columnsWidth) {
            this.container.setStyle("left:0px");
            let margin = this.table.getWidth() - columnsWidth;
            let individualMargin = margin / this.columns.length;

            this.container.setStyle("width:" + (this.table.getWidth()) + "px");

            for (let i = 0; i < this.columns.length; i++) {
                this.columns[i].addWidth(individualMargin);
            }
        }

        this.hideSliding();
    }

    resizeColumns() {
        for (let i = 0; i < this.columns.length; i++) {
            this.columns[i].resize();
        }
    }

    getColumnsWidth() {
        let columnsWidth = 0;
        for (let i = 0; i < this.columns.length; i++) {
            let column = this.columns[i];
            columnsWidth += column.getWidth();
        }

        return columnsWidth;
    }

    remove() {
        this.clear();
        this.container.remove();
        this.container = null;

        this.slidingRightButton = null;
        this.slidingLeftButton = null;

        this.columns = [];
    }
}