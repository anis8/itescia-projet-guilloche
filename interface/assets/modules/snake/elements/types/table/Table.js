import {Element} from "../../Element.js";
import {TableHeader} from "./types/TableHeader.js";
import {TableContainer} from "./types/TableContainer.js";

export class Table extends Element {

    constructor(element, storage) {
        super(element, storage);
        this.data = {
            columns: [
                {
                    id: "name",
                    order: 1,
                    orderBy: null,
                    visible: true,
                    name: "Text",
                    width: "300px",
                    type: "text",
                }, {
                    id: "color",
                    order: 2,
                    orderBy: null,
                    visible: true,
                    name: "Status",
                    width: "20%",
                    type: "color",
                }, {
                    id: "lastName",
                    order: 2,
                    orderBy: null,
                    visible: true,
                    name: "Number",
                    width: "20%",
                    type: "number",
                }, {
                    id: "rating",
                    order: 3,
                    orderBy: null,
                    visible: true,
                    name: "Rating",
                    width: "20%",
                    type: "number",
                }, {
                    id: "description",
                    order: 4,
                    orderBy: null,
                    visible: true,
                    name: "Description",
                    width: "20%",
                    type: "number",
                },
            ],
            entries: [
                {id: 1, name: "A name", lastName: 1, rating: 3, color: "red", description: 84},
                {id: 2, name: "B name", lastName: 2, rating: 5, color: "blue", description: 18},
                {id: 3, name: "C name", lastName: 3, rating: 1, color: "red", description: 50},
                {id: 4, name: "X name", lastName: 99, rating: 2, color: "blue", description: 48},
                {id: 5, name: "Y name", lastName: 5, rating: 4, color: "blue", description: 34},
                {id: 6, name: "Z name", lastName: 6, rating: 3, color: "red", description: 19},
            ]
        };
        this.header = new TableHeader(this);
        this.container = new TableContainer(this);


    }

    destroy() {
        super.destroy();

        this.header.remove();
        this.container.remove();
        this.data = {};

        this.data = null;
        this.container = null;
        this.header = null;

    }
}