import {Snake} from "../Snake.js";
import {SnakeMachine} from "../utilities/SnakeMachine.js";

export class Element {

    constructor(element, storage) {
        this.element = element;
        this.listeners = [];
        this.timeOuts = [];
        this.intervals = [];
        this.childrens = [];
        if (typeof storage === "undefined") {
            Snake.store(this);
        }
    };

    frame(startValue, goalValue, speed, func, round) {
        let start = performance.now();
        requestAnimationFrame(function animate(time) {
            let timeFraction = (time - start) / speed;
            if (timeFraction > 1) timeFraction = 1;
            let progress = startValue + (timeFraction * (goalValue - startValue));
            if (typeof round === "undefined") {
                progress = Math.round(progress);
            }
            func(progress);
            if (timeFraction < 1) {
                requestAnimationFrame(animate);
            }
        });
    };

    animateLeft(value, speed) {
        let self = this;
        this.frame(this.getParentOffsets().left, this.getParentOffsets().left + value, speed, function (val) {
            self.element.style.left = val + "px";
        });
        this.element.style.right = null;
    };

    animateTop(value, speed) {
        let self = this;
        this.frame(this.getParentOffsets().top, this.getParentOffsets().top + value, speed, function (val) {
            self.element.style.top = val + "px";
        });
        this.element.style.bottom = null;
    };

    pushTop(value, speed) {
        let self = this;
        if (self.element.style.top === "") {
            self.element.style.top = "0px";
        }
        let start = parseInt(self.element.style.top);
        this.frame(start, start + value, speed, function (val) {
            self.element.style.top = val + "px";
        });
        this.element.style.bottom = null;
    };

    animateRight(value, speed) {
        let self = this;
        this.frame(this.getParentOffsets().right, this.getParentOffsets().right + value, speed, function (val) {
            self.element.style.right = val + "px";
        });
        this.element.style.left = null;
    };

    animateBottom(value, speed) {
        let self = this;
        this.frame(this.getParentOffsets().bottom, this.getParentOffsets().bottom + value, speed, function (val) {
            self.element.style.bottom = val + "px";
        });
        this.element.style.top = null;
    };

    animateWidth(value, speed) {
        let self = this;
        this.frame(this.getWidth(), value, speed, function (val) {
            self.element.style.width = val + "px";
        });
    };

    animateHeight(value, speed) {
        let self = this;
        this.frame(this.getHeight(), value, speed, function (val) {
            self.element.style.height = val + "px";
        });
    };

    moveLeft(value, speed) {
        let self = this;
        this.frame(this.getParentOffsets().left, value, speed, function (val) {
            self.element.style.left = val + "px";
        });
        this.element.style.right = null;
    };

    moveTop(value, speed) {
        let self = this;
        this.frame(this.getParentOffsets().top, value, speed, function (val) {
            self.element.style.top = val + "px";
        });
        this.element.style.bottom = null;
    };

    moveRight(value, speed) {
        let self = this;
        this.element.style.left = null;
        this.frame(this.getParentOffsets().right, value, speed, function (val) {
            self.element.style.right = val + "px";
        });
    };

    moveBottom(value, speed) {
        let self = this;
        this.frame(150, value, speed, function (val) {
            self.element.style.bottom = val + "px";
        });
        this.element.style.top = null;
    };

    scale(start, value, speed) {
        let self = this;
        self.element.style.transform = "scale(" + start + ")";
        this.frame(start, value, speed, function (val) {
            if (self.element != null) {
                self.element.style.transform = "scale(" + val + ")";
            }
        }, false);
    };

    fade(value, speed) {
        let self = this;
        self.element.style.opacity = (value === "in" ? "0" : "1");
        let start = (value === "in" ? 0 : 1);
        let goal = (value === "in" ? 1 : 0);
        if (value === "in") {
            self.show();
        }
        this.frame(start, goal, speed, function (val) {
            if (self.element != null) {
                self.element.style.opacity = val;
                if (val === goal) {
                    if (value === "out") {
                        self.hide();
                    }
                }
            }
        }, false);

    };

    draggable(element) {
        element = (typeof element !== "undefined" ? element : this);
        let self = this;
        let initX, initY, mousePressX, mousePressY;
        let down = false;

        let tMoveListener = null;
        let tUpListener = null;

        let touchDown = function (e) {
            e.preventDefault();
            initX = element.element.offsetLeft;
            initY = element.element.offsetTop;
            let touch = e.touches;
            mousePressX = touch[0].pageX;
            mousePressY = touch[0].pageY;
            self.addListener('touchmove', move);
            tMoveListener = self.putListener(window, 'touchend', touchUp);
            tUpListener = self.putListener(window, 'touchend', touchUp);
        };
        let touchUp = function (e) {
            e.preventDefault();
            self.removeListener('touchmove', move);
            self.deleteListener(tMoveListener);
            self.deleteListener(tUpListener);
        };


        let moveListener = null;
        let upListener = null;

        let mouseDown = function (event) {
            initX = element.element.offsetLeft;
            initY = element.element.offsetTop;
            mousePressX = event.pageX;
            mousePressY = event.pageY;
            down = true;
            moveListener = self.putListener(window, 'mousemove', move);
            upListener = self.putListener(window, 'mouseup', mouseUp);
        };
        let mouseUp = function () {
            if (down) {
                down = false;
                self.deleteListener(moveListener);
                self.deleteListener(upListener);
            }
        };


        self.addListener('touchstart', touchDown);
        self.addListener('mousedown', mouseDown);

        function move(event) {
            if (down) {
                let left = initX + ((typeof event.touches !== "undefined") ? event.touches[0].pageX : event.pageX) - mousePressX;
                let top = initY + ((typeof event.touches !== "undefined") ? event.touches[0].pageY : event.pageY) - mousePressY;

                if (left >= 0 && left <= (window.innerWidth - self.getWidth())) {
                    element.element.style.left = left + 'px';
                }

                if (top >= 0 && top <= (window.innerHeight - self.getHeight())) {
                    element.element.style.top = top + 'px';
                }
            }
        }
    };

    getScrollTop() {
        return this.element.scrollTop;
    }

    getScrollBottom() {
        return (this.element.scrollHeight - Math.round(this.element.scrollTop + this.getHeight()));
    }

    onScroll(callback) {
        return this.addListener("scroll", callback);
    }

    scrollTop(value, speed) {
        let self = this;
        let easeInOutQuad = function (t, b, c, d) {
            t /= d / 2;
            if (t < 1) return c / 2 * t * t + b;
            t--;
            return -c / 2 * (t * (t - 2) - 1) + b;
        };
        let start = this.element.scrollTop,
            change = value - start,
            currentTime = 0,
            increment = 20;
        let animateScroll = function () {
            currentTime += increment;
            self.element.scrollTop = easeInOutQuad(currentTime, start, change, speed);
            if (currentTime < speed) {
                SnakeMachine.setTimeout(animateScroll, increment);
            }
        };
        animateScroll();
    };

    scrollBottom(value, speed) {
        let self = this;
        let startDistance = this.element.scrollTop;
        let endDistance = (this.element.scrollHeight - this.getHeight()) - value;
        let frame = 0;
        let delta = (endDistance - startDistance) / speed / 0.06;
        let handle = SnakeMachine.setInterval(function () {
            frame++;
            let value = startDistance + delta * frame;
            self.element.scrollTo(0, value);
            if (value >= endDistance) {
                clearInterval(handle);
            }
        }, 1 / 0.06);

    };

    getHeight() {
        return this.element.clientHeight;
    };

    getWidth() {
        return this.element.clientWidth;
    };

    getParentOffsets() {
        let right = 0;
        let bottom = 0;
        if (this.element.parentNode !== null) {
            right = this.element.parentNode.clientWidth - (this.element.offsetLeft + this.getWidth());
            bottom = this.element.parentNode.clientHeight - (this.element.offsetTop + this.getHeight());
        }
        return {top: this.element.offsetTop, left: this.element.offsetLeft, right: right, bottom: bottom}
    }

    getOffsets() {
        let offsets = this.element.getBoundingClientRect();
        return {
            top: offsets.top,
            left: offsets.left,
            right: window.innerWidth - (offsets.left + this.getWidth()),
            bottom: window.innerHeight - (offsets.top + this.getHeight())
        };
    };

    getValue() {
        return this.element.value;
    }

    setStyle(value) {
        this.element.style.cssText += value;
    };

    getStyle(value) {
        return this.element.style[value];
    }

    hide() {
        this.isHide = true;
        this.setStyle("visibility:hidden;");
    };

    show() {
        this.isHide = false;
        this.setStyle("visibility:visible;");
    };

    displayHide() {
        this.isHide = true;
        this.setStyle("display:none;");
    };

    displayShow() {
        this.isHide = false;
        this.setStyle("display:block;");
    };

    onResize(func) {
        let self = this;
        let width = this.getWidth();
        let height = this.getHeight();
        this.interval(function () {
            if (width !== self.getWidth() || height !== self.getHeight()) {
                width = self.getWidth();
                height = self.getHeight();
                func();
            }
        }, 25);
    };

    prepend(html) {
        let buffer = document.createElement('div');
        buffer.innerHTML = html.trim();
        let element = buffer.firstChild;

        this.element.appendChild(element);
        this.element.insertBefore(element, this.element.firstChild);

        let snk = Snake.el(element);
        this.addChildren(snk);
        return snk;
    };

    append(html) {
        let buffer = document.createElement('div');
        buffer.innerHTML = html.trim();
        let element = buffer.firstChild;

        this.element.appendChild(element);
        this.element.insertBefore(element, this.element.lastChild);

        let snk = Snake.el(element);
        this.addChildren(snk);
        return snk;
    };

    addChildren(element) {
        if (!this.childrens.includes(element)) {
            this.childrens.push(element);
        }
    }

    save() {
        this.savedStyle = this.element.getAttribute("style");
        this.savedHtml = this.element.innerHTML;
    };

    restore() {
        this.element.setAttribute("style", this.savedStyle);
        this.element.innerHTML = this.savedHtml;
    };

    copy() {
        let elem = this.element.cloneNode(true);
        elem.setAttribute('id', "snk-copy-" + Math.random() + "-" + Math.random());
        let snkElem = new Element(elem);

        let styles = window.getComputedStyle(this.element);
        if (styles.cssText !== '') {
            elem.style.cssText = styles.cssText;
        } else {
            elem.style.cssText = Object.values(styles).reduce(
                (css, propertyName) =>
                    `${css}${propertyName}:${styles.getPropertyValue(
                        propertyName
                    )};`
            )
        }
        return snkElem;
    };

    getHtml() {
        return this.element.innerHTML;
    };

    getText() {
        return this.element.innerText;
    };

    setHtml(html) {
        this.element.innerHTML = html;
    };

    getParents() {
        let nodes = [];
        let element = this.element;
        nodes.push(element);
        while (element.parentNode) {
            nodes.unshift(element.parentNode);
            element = element.parentNode;
        }
        return nodes;
    }

    onclick(func) {

        this.element.onclick = func;
    };

    dynamicOnclick(func) {
        let self = this;
        /*
        let globalFunc = function (e) {
            if (typeof self.isHide === "undefined" || self.isHide === false) {
                if (e.target === self.element) {
                    func(e);
                } else {
                    let nodes = [];
                    let element = e.target;
                    nodes.push(element);
                    while (element.parentNode) {
                        nodes.unshift(element.parentNode);
                        element = element.parentNode;
                    }
                    nodes.reverse();
                    for (let i = 0; i < nodes.length; i++) {
                        let parent = nodes[i];
                        if (parent !== document) {
                            if (parent === self.element) {
                                func(e);
                            }
                            if (parent.getAttribute("snkclick") != null) {
                                break;
                            }
                        }
                    }
                }
            }
        };
        this.addAttribute("snkclick", true);

         */
        return this.addListener('click', function(e){
            e.stopPropagation();
            func();
        });
    }

    onsubmit(func) {
        this.element.onsubmit = function (e) {
            func(e);
        }
    }

    clearListeners() {
        for (let i = 0; i < this.listeners.length; i++) {
            SnakeMachine.removeListener(this.listeners[i]);
        }
        this.listeners = [];
    };

    clearTimeouts() {
        for (let i = 0; i < this.timeOuts.length; i++) {
            clearTimeout(this.timeOuts[i]);
        }
        this.timeOuts = [];
    }

    clearIntervals() {
        for (let i = 0; i < this.intervals.length; i++) {
            clearInterval(this.intervals[i]);
        }
        this.intervals = [];
    }

    addListener(name, func) {
        return this.putListener(this.element, name, func);
    }

    removeListener(name, func) {
        this.deleteListener([this.element, name, func]);
    }

    putListener(element, name, func) {
        let listener = SnakeMachine.addListener(element, name, func);
        this.listeners.push(listener);
        return listener;
    }

    deleteListener(data) {
        SnakeMachine.removeListener(data);
        let index = this.listeners.indexOf(data);
        if (index >= 0) {
            this.listeners.splice(index, 1);
        }
    }


    hasClass(cls) {
        return !!this.element.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
    };

    addClass(cls) {
        if (!this.hasClass(cls)) this.element.className += " " + cls;
    };

    getAttribute(name){
        return this.element.getAttribute(name);
    }

    addAttribute(name, content) {
        this.element.setAttribute(name, content);
    };

    removeAttribute(name) {
        this.element.removeAttribute(name);
    };

    removeClass(cls) {
        if (this.hasClass(cls)) {
            let reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
            this.element.className = this.element.className.replace(reg, ' ');
        }
    };

    el(className) {
        let e = Snake.el(this.hasClass(className) ? this.element : this.element.getElementsByClassName(className)[0]);
        this.addChildren(e);
        return e;
    }

    get(className) {
        return Snake.get(this.hasClass(className) ? this.element : this.element.getElementsByClassName(className)[0]);
    }

    ed(className) {
        let e = Snake.ed(this.hasClass(className) ? this.element : this.element.getElementsByClassName(className)[0]);
        this.addChildren(e);
        return e;
    }

    tab(className) {
        let e = Snake.tab(this.hasClass(className) ? this.element : this.element.getElementsByClassName(className)[0]);
        this.addChildren(e);
        return e;
    }

    has(className) {
        return !this.get(className).isNull();
    }

    delay(callback, ms) {
        let timer = 0;
        let self = this;
        return function () {
            let context = this, args = arguments;
            clearTimeout(timer);
            timer = SnakeMachine.setTimeout(function () {
                if (!self.specialKey) {
                    callback.apply(context, args);
                }
            }, ms || 0);
        };
    };

    timeOut(callback, ms) {
        let timeout = SnakeMachine.setTimeout(function () {
            callback();
        }, ms);
        this.timeOuts.push(timeout);
    }

    interval(callback, ms) {
        callback();
        let timeout = SnakeMachine.setInterval(function () {
            callback();
        }, ms);
        this.intervals.push(timeout);
    }

    isNull() {
        return this.element == null;
    }

    clearChildrens() {
        for (let i = 0; i < this.childrens.length; i++) {
            this.childrens[i].destroy();
        }
        this.childrens = [];
    }

    remove() {
        if (this.element.parentNode !== null) {
            this.element.parentNode.removeChild(this.element);
        }

        this.destroy();
    };

    destroy() {
        Snake.unStore(this);
        this.clearListeners();
        this.clearIntervals();
        this.clearTimeouts();
        this.clearChildrens();

        this.element = null;
        delete this;
    }

    clear() {
        this.element.innerHTML = "";
        this.clearListeners();
        this.clearIntervals();
        this.clearTimeouts();
        this.clearChildrens();
    };

}