import {Network} from "../Network.js";

export class HttpFileDownloader {

    constructor(files) {
        this.urls = [];
        if (typeof files !== "undefined") {
            this.urls = files;
        }

        this.requests = [];

    }

    stop() {
        for (let i = 0; i < this.requests.length; i++) {
            this.requests[i].abort();
        }
    }

    startDownload(data, end, error, progress) {
        let promises = [];
        let progressions = {};
        let downloads = {};
        let self = this;
        for (let i = 0; i < this.urls.length; i++) {
            let url = this.urls[i];
            progressions[url] = 0;
            promises.push(new Promise(((resolve) => {
                let request = Network.fileDownload({
                    type: "GET",
                    url: url,
                    data: data,
                    success: function (e) {
                        downloads[url] = new File([e], url, {type: e.type, lastModified: new Date()});
                        resolve();
                    },
                    error: function () {
                        error();
                    },
                    progress: function (e) {
                        progressions[url] = e;
                        let total = 0;
                        for (let key in progressions) {
                            total += progressions[key];
                        }
                        progress(Math.round(total / self.urls.length));
                    },
                    end: function () {
                        resolve();
                    },
                });
                self.requests.push(request);
            })));
        }
        Promise.all(promises).then(() => {
            self.requests = [];
            end(downloads);
        });
    }
}