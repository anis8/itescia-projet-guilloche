import {Network} from "../Network.js";

export class HttpFileUploader {

    constructor(files) {
        this.files = files;
    }

    startUpload(data, end, error, progress) {
        let files = this.files;
        let promises = [];
        let progressions = {};
        for (let i = 0; i < files.length; i++) {
            progressions[files[i].name] = 0;
            promises.push(new Promise(((resolve) => {
                Network.file({
                    type: "POST",
                    pathname: "upload",
                    file: files[i],
                    data: data,
                    success: function (e) {
                        resolve(e);
                    },
                    error: function () {
                        error();
                    },
                    progress: function (e) {
                        progressions[files[i].name] = e;
                        let values = [];
                        let total = 0;
                        for (let i = 0; i < files.length; i++) {
                            let value = progressions[files[i].name];
                            if (typeof value !== "undefined") {
                                total += value;
                                values.push([files[i], value]);
                            }

                        }

                        progress(Math.round(total / files.length), values);
                    },
                    end: function (e) {
                        resolve(e);
                    },
                });
            })));
        }
        Promise.all(promises).then((e) => {
            end(e);
        });
    }

    static isSecure(name) {
        let types = ["htm", "jpg", "mpg", "tgz", "xml", "7z", "zip", "svg", "wav", "xls", "sql", "torrent", "tar", "rar", "ppt", "pps", "png", "pdf", "odg", "ods", "odp", "odt", "mp4", "mp3", "mkv", "jpeg", "ico", "iso", "gz", "gif", "docx", "doc", "avi", "log", "pptx", "pem", "log", "txt"];
        return types.includes(name);
    }

    static getExtension(name) {
        name = name.toLowerCase();
        let two = name.substring(name.length - 2);
        let three = name.substring(name.length - 3);
        let four = name.substring(name.length - 4);
        if (four.includes(".")) {
            if (three.includes(".")) {
                return two;
            } else {
                return three;
            }
        } else {
            return four;
        }
    }

    static isImage(type) {
        switch (type) {
            case "image/gif":
            case "image/png":
            case "image/jpeg":
            case "image/bmp":
            case "image/webp":
            case "image/svg+xml":
            case "gif":
            case "png":
            case "jpeg":
            case "jpg":
            case "ico":
            case "svg":
            case "bmp":
            case "webp":
                return true;
        }
        return false;
    }
}