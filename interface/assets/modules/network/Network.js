
import {HttpFileUploader} from "./http/HttpFileUploader.js";
import {Session} from "../objects/session/Session.js";
import {App} from "../App.js";

export class Network {

    static async init() {
        this.images = "localhost/Fingerprint/uploads";
        this.httpHost = "http://127.0.0.1:8090";
        await this.connect();
    }

    static request(req) {
        App.sn.http().request(req);
    }

    static basicRequest(req) {
        App.sn.http().sendRequest(req);
    }

    static fileRequest(files) {
        return new HttpFileUploader(files);
    }

    static fileDownload(req) {
        return App.sn.http().download(req);
    }

    static file(req) {
        App.sn.http().file(req);
    }

    static async connect() {
        let self = this;
        return new Promise(((resolve) => {
            App.sn.http().bind(self.httpHost, 0, {"crossOrigin": true, "dataType": "json"});

            this.request({
                type: "POST",
                pathname: "ping",
                success: async function (e) {
                    Session.online = e.connect;
                    Session.token = e.session;
                    Session.save();
                    resolve();
                },
                error: function () {
                    console.log("HTTP error")
                }
            });
        }));
    }
}