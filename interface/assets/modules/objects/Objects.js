
import {Session} from "./session/Session.js";
import {Cache} from "./cache/Cache.js";

export class Objects {
    static async init() {
        await Cache.init();
        await Promise.all([
            Session.init(),
        ]);
    }
}