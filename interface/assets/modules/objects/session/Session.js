import {Cache} from "../cache/Cache.js";

export class Session {

    static async init() {
        this.online = Cache.get("online");
        this.token = Cache.get("token");
    }



    static isOnline() {
        return this.online == null ? false : this.online;
    }

    static save() {
        Cache.set("online", this.online);
        Cache.set("token", this.token);
    }


}