export class Cache {


    static async init() {
        this.version = "d";
        this.persistent = {
            version: this.version,
            connect: false,
            session: null
        };

        let persistent = localStorage.getItem('persistent');
        if (persistent !== null) {
            persistent = JSON.parse(persistent);
            if (persistent.version === this.version) {
                this.persistent = persistent;
            }
        }
    }

    static save() {
        localStorage.setItem('persistent', JSON.stringify(this.persistent));
    }

    static get(key) {
        if (typeof this.persistent[key] !== "undefined") {
            return this.persistent[key];
        }
        return null;
    }

    static set(key, value) {
        this.persistent[key] = value;
    }


    static clear() {
        this.persistent = {
            version: this.version
        };
    }
}