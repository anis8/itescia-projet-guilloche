package source.network;

import io.netty.bootstrap.ServerBootstrap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import source.interfaces.initialisable.IInitialisable;
import source.network.http.HttpServer;

import java.net.InetSocketAddress;

public class NetworkManager implements IInitialisable {
    private static final Logger log = LogManager.getLogger(NetworkManager.class);
    private static NetworkManager networkManagerInstance;
    private HttpServer httpServer;

    public static NetworkManager getInstance() {
        if (networkManagerInstance == null)
            networkManagerInstance = new NetworkManager();

        return networkManagerInstance;
    }

    @Override
    public void initialise() {
        this.httpServer = new HttpServer();
    }

    protected void bind(ServerBootstrap bootstrap, String ip, int port, String name) {
        try {
            bootstrap.bind(new InetSocketAddress(ip, port)).addListener(objectFuture -> {
                if (!objectFuture.isSuccess()) {
                    log.error("Error during server initialization ! : " + ip + ":" + port);
                }
            });
            log.info("Listening  " + name + " on ip: " + ip + " port: " + port);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error during server initialization ! : " + ip + ":" + port);
        }
    }

    public HttpServer getHttpServer() {
        return httpServer;
    }


    public void shutdown() {
        this.httpServer.shutdown();
        log.info("Shutdown !");
    }
}
