package source.network.sessions;

import java.util.HashMap;

public abstract class Session {

    private HashMap<String, Object> attributes = new HashMap<>();

    public Object getAttribute(String name) {
        return this.attributes.get(name);
    }

    public void addAttribute(String name, Object attribute) {
        this.attributes.put(name, attribute);
    }

    public boolean hasAttribute(String name) {
        return this.attributes.containsKey(name);
    }

    public void removeAttribute(String name) {
        this.attributes.remove(name);
    }

}
