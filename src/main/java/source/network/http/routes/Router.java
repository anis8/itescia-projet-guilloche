package source.network.http.routes;

import source.interfaces.network.http.IRoute;
import source.network.http.routes.incoming.handshake.DetectionsRoute;
import source.network.http.routes.incoming.handshake.PingRoute;
import source.network.http.routes.incoming.upload.UploadRoute;

import java.util.concurrent.ConcurrentHashMap;

public class Router {

    private ConcurrentHashMap<String, IRoute> routes;

    public Router() {
        this.routes = new ConcurrentHashMap<>();
        this.loadRoutes();
    }

    public void loadRoutes() {
        this.addRoute("/ping", new PingRoute());
        this.addRoute("/upload", new UploadRoute());
        this.addRoute("/detections", new DetectionsRoute());
    }

    public boolean hasRoute(String route) {
        return this.routes.containsKey(route);
    }

    public IRoute getRoute(String route) {
        return this.routes.get(route);
    }

    public void addRoute(String path, IRoute route) {
        this.routes.put(path, route);
    }
}
