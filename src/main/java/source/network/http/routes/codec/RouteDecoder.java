package source.network.http.routes.codec;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.QueryStringDecoder;
import io.netty.handler.codec.http.cookie.Cookie;
import io.netty.handler.codec.http.cookie.ServerCookieDecoder;
import io.netty.handler.codec.http.multipart.*;
import source.utlities.uploader.Uploader;

import java.nio.charset.StandardCharsets;
import java.util.*;

public class RouteDecoder {
    private Map<String, Object> post;
    private Map<String, Object> get;
    private ArrayList<FileUpload> files;
    private String content;
    private Map<String, Object> cookies;
    private HttpPostRequestDecoder postDecoder = null;
    private QueryStringDecoder getDecoder = null;
    private ChannelHandlerContext ctx;

    public RouteDecoder(FullHttpRequest req, ChannelHandlerContext ctx) throws Exception {
        this.ctx = ctx;
        this.post = new HashMap<>();
        this.get = new HashMap<>();
        this.files = new ArrayList<>();
        this.cookies = new HashMap<>();
        this.content = req.content().toString(StandardCharsets.UTF_8);
        HttpMethod method = req.method();
        String cookieHeader = req.headers().get(HttpHeaderNames.COOKIE);
        if (cookieHeader != null) {
            Set<Cookie> cookies = ServerCookieDecoder.LAX.decode(cookieHeader);
            for (Cookie cookie : cookies) {
                this.cookies.put(cookie.name(), cookie.value());
            }
        }

        if (HttpMethod.GET == method) {
            this.getDecoder = new QueryStringDecoder(req.uri());
            this.getDecoder.parameters().forEach((key, value) -> this.get.put(key, value.get(0)));

        } else if (HttpMethod.POST == method) {
            this.postDecoder = new HttpPostRequestDecoder(new DefaultHttpDataFactory(true), req);
            this.postDecoder.setDiscardThreshold(0);
            this.postDecoder.offer(req);

            List<FileUpload> files = new ArrayList<>();
            List<InterfaceHttpData> paramList = this.postDecoder.getBodyHttpDatas();
            for (InterfaceHttpData param : paramList) {
                if (param.getHttpDataType() == InterfaceHttpData.HttpDataType.FileUpload) {
                    FileUpload file = (FileUpload) param;
                    String fileName = file.getFilename();
                    if (Uploader.isSecureFile(fileName)) {
                        this.files.add(file);
                    }
                } else {
                    Attribute data = (Attribute) param;
                    this.post.put(data.getName(), data.getValue());
                }
            }
        }
    }

    public void destroy() {
        if (this.postDecoder != null) {
            this.postDecoder.destroy();
        }
    }

    public ArrayList<FileUpload> getFiles() {
        return files;
    }

    public Map<String, Object> getPost() {
        return post;
    }

    public HttpPostRequestDecoder getPostDecoder() {
        return postDecoder;
    }

    public QueryStringDecoder getGetDecoder() {
        return getDecoder;
    }

    public ChannelHandlerContext getCtx() {
        return ctx;
    }

    public Map<String, Object> getGet() {
        return get;
    }

    public Map<String, Object> getCookies() {
        return cookies;
    }

    public String getContent() {
        return content;
    }
}