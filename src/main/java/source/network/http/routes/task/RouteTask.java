package source.network.http.routes.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import source.interfaces.network.http.IRoute;
import source.interfaces.task.ITask;
import source.network.http.routes.codec.RouteDecoder;
import source.network.http.sessions.HSession;


public class RouteTask implements ITask {
    private static final Logger log = LogManager.getLogger(RouteTask.class.getName());
    private HSession session;
    private IRoute route;
    private RouteDecoder decodedRoute;

    public RouteTask(HSession session, IRoute route, RouteDecoder decodedRoute) {
        this.session = session;
        this.route = route;
        this.decodedRoute = decodedRoute;
    }

    @Override
    public void run() {
        try {
            this.route.handle(this.session, this.decodedRoute);
            this.decodedRoute.destroy();
        } catch (Exception e) {
            log.error("Error during handle route: " + this.route.getClass().getSimpleName() + " ", e);
        }
    }

}
