package source.network.http.routes.incoming.handshake;

import io.netty.handler.codec.http.HttpResponseStatus;
import org.json.JSONObject;
import source.interfaces.network.http.IRoute;
import source.network.http.HttpHandler;
import source.network.http.routes.codec.RouteDecoder;
import source.network.http.sessions.HSession;


public class PingRoute implements IRoute {
    @Override
    public void handle(HSession session, RouteDecoder decoder) {
        JSONObject obj = new JSONObject();
        obj.put("session", session.getToken());
        obj.put("connect", false);
        session.writeResponse(decoder.getCtx(), HttpResponseStatus.OK, this.getContentType(), obj.toString());
    }

    public String getContentType() {
        return HttpHandler.TYPE_JSON;
    }
}
