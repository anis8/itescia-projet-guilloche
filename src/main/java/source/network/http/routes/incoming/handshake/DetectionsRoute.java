package source.network.http.routes.incoming.handshake;

import io.netty.handler.codec.http.HttpResponseStatus;
import org.json.JSONObject;
import source.detection.DetectionManager;
import source.interfaces.network.http.IRoute;
import source.network.http.HttpHandler;
import source.network.http.routes.codec.RouteDecoder;
import source.network.http.sessions.HSession;

import java.util.Map;


public class DetectionsRoute implements IRoute {
    @Override
    public void handle(HSession session, RouteDecoder decoder) {
        JSONObject object = new JSONObject();

        for (Map.Entry<String, String> entry : DetectionManager.getInstance().getDetectionNames().entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            object.put(key, value);
        }


        session.writeResponse(decoder.getCtx(), HttpResponseStatus.OK, this.getContentType(), object.toString());
    }

    public String getContentType() {
        return HttpHandler.TYPE_JSON;
    }
}
