package source.network.http.routes.incoming.upload;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.multipart.FileUpload;
import org.json.JSONObject;
import source.detection.Detection;
import source.detection.DetectionExecution;
import source.detection.DetectionManager;
import source.interfaces.network.http.IRoute;
import source.network.http.HttpHandler;
import source.network.http.routes.codec.RouteDecoder;
import source.network.http.sessions.HSession;

public class UploadRoute implements IRoute {
    @Override
    public void handle(HSession session, RouteDecoder decoder) {
        JSONObject obj = new JSONObject();
        obj.put("success", false);
        if (decoder.getFiles().size() > 0) {
            for (FileUpload file : decoder.getFiles()) {
                try {
                    String typeName = (String) decoder.getPost().get("typeName");
                    Detection detection = DetectionManager.getInstance().createDetection(typeName, file);
                    if (detection != null) {
                        DetectionExecution execution = new DetectionExecution(
                                session,
                                decoder,
                                HttpResponseStatus.OK,
                                detection
                        );

                        DetectionManager.getInstance().executeDetection(execution);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        session.writeResponse(decoder.getCtx(), HttpResponseStatus.OK, this.getContentType(), obj.toString());
    }

    public String getContentType() {
        return HttpHandler.TYPE_JSON;
    }
}
