package source.network.http;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelOption;
import io.netty.channel.DefaultMessageSizeEstimator;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import source.boot.Rocket;
import source.network.NetworkManager;
import source.network.http.routes.Router;
import source.network.http.sessions.HSessionManager;


public class HttpServer extends NetworkManager {

    private static final Logger log = LogManager.getLogger(HttpServer.class);
    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;
    private Router router;
    private HSessionManager sessions;

    public HttpServer() {
        this.sessions = new HSessionManager();
        this.router = new Router();
        this.bossGroup = new NioEventLoopGroup(Integer.parseInt(Rocket.getConfig().get("http.boss.pool")));
        this.workerGroup = new NioEventLoopGroup(Integer.parseInt(Rocket.getConfig().get("http.worker.pool")));
        try {

            ServerBootstrap b = new ServerBootstrap()
                    .group(this.bossGroup, this.workerGroup)
                    .childHandler(new HttpInitializer())
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG, 1024) //connexion max file d'attente
                    .option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT) //à vérif
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 1000)
                    .option(ChannelOption.MESSAGE_SIZE_ESTIMATOR, DefaultMessageSizeEstimator.DEFAULT) //à vérif
                    .childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
            this.bind(b, Rocket.getConfig().get("http.host"), Integer.parseInt(Rocket.getConfig().get("http.port")), "HttpServer");
        } catch (Exception e) {
            log.error("Error !", e);
            Rocket.exit("Error during HTTP http.server initialization");
        }
    }

    Router getRouter() {
        return this.router;
    }

    public void shutdown() {
        this.bossGroup.shutdownGracefully();
        this.workerGroup.shutdownGracefully();
    }

    public HSessionManager getSessions() {
        return sessions;
    }
}
