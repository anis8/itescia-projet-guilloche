package source.network.http;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.multipart.DiskAttribute;
import io.netty.handler.codec.http.multipart.DiskFileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import source.interfaces.network.http.IRoute;
import source.network.NetworkManager;
import source.network.http.routes.codec.RouteDecoder;
import source.network.http.routes.task.RouteTask;
import source.network.http.sessions.HSession;
import source.task.TaskManager;
import source.utlities.Encryption;

public class HttpHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

    public static final String TYPE_PLAIN = "text/plain; charset=UTF-8";
    public static final String TYPE_JSON = "text/json; charset=UTF-8";
    public static final String TYPE_HTML = "text/html; charset=UTF-8";
    public static final Logger log = LogManager.getLogger(HttpHandler.class);

    static {
        DiskFileUpload.deleteOnExitTemporaryFile = true;
        DiskFileUpload.baseDirectory = null;
        DiskAttribute.deleteOnExitTemporaryFile = true;
        DiskAttribute.baseDirectory = null;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest msg) {
        final String[] uri = msg.uri().split("\\?");
        try {
            RouteDecoder routeDecoded = new RouteDecoder(msg, ctx);
            String token = (String) routeDecoded.getCookies().get("sessionToken");
            HSession session;
            if (token != null && NetworkManager.getInstance().getHttpServer().getSessions().getSessions().containsKey(token)) {
                session = NetworkManager.getInstance().getHttpServer().getSessions().getSessions().get(token);
            } else {
                session = this.connectSession();
            }

            if (!NetworkManager.getInstance().getHttpServer().getRouter().hasRoute(uri[0])) {
                session.writeErrorResponse(ctx, HttpResponseStatus.NOT_FOUND);
            } else {
                final IRoute route = NetworkManager.getInstance().getHttpServer().getRouter().getRoute(uri[0]);
                TaskManager.getInstance().execute(new RouteTask(session, route, routeDecoded));
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error during running a route: ", e);
        }
    }

    private HSession connectSession() {
        HSession session = new HSession(Encryption.generateBigKey());
        session.markTokenUpdate();
        NetworkManager.getInstance().getHttpServer().getSessions().add(session);
        return session;
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }


}