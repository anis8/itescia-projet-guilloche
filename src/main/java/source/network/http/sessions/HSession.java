package source.network.http.sessions;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.cookie.Cookie;
import io.netty.handler.codec.http.cookie.DefaultCookie;
import io.netty.handler.codec.http.cookie.ServerCookieEncoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import source.network.http.HttpHandler;
import source.network.sessions.Session;


import java.nio.charset.StandardCharsets;


public class HSession extends Session {
    private final Logger logger = LogManager.getLogger(HSession.class);
    private final String token;
    private boolean tokenUpdate = false;

    public HSession(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }



    public void writeErrorResponse(final ChannelHandlerContext ctx, final HttpResponseStatus status) {
        writeResponse(ctx, status, HttpHandler.TYPE_PLAIN, status.reasonPhrase());
    }

    public void writeResponse(final ChannelHandlerContext ctx, final HttpResponseStatus status, final CharSequence contentType, final String content) {
        ByteBuf buffer = Unpooled.copiedBuffer(content, StandardCharsets.UTF_8);
        final FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, status, buffer, false);
        final DefaultHttpHeaders headers = (DefaultHttpHeaders) response.headers();
        headers.set(HttpHeaderNames.CONTENT_TYPE, contentType);
        headers.set(HttpHeaderNames.CONTENT_LENGTH, Integer.toString(response.content().readableBytes()));
        headers.set("Access-Control-Allow-Origin", "https://fingerprint.anistriki.fr");
        headers.set("Access-Control-Allow-Credentials", "true");
        if (this.tokenUpdate) {
            Cookie cookie = new DefaultCookie("sessionToken", this.getToken());
            cookie.setHttpOnly(true);
            cookie.setPath("/");
            cookie.setMaxAge(1300000);
            headers.set("Set-Cookie", ServerCookieEncoder.STRICT.encode(cookie));
        }
        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }


    public void markTokenUpdate() {
        this.tokenUpdate = true;
    }
}
