package source.network.http.sessions;
import source.network.sessions.SessionManager;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public final class HSessionManager extends SessionManager {
    private final Map<String, HSession> sessions = new ConcurrentHashMap<>();

    public boolean add(HSession session) {
        return (this.sessions.putIfAbsent(session.getToken(), session) == null);
    }

    public void remove(String token) {
        this.sessions.remove(token);
    }


    public void periodicClear() {
        //TODO periodic clear un visitor sites
    }


    public HSession get(String token) {
        return this.sessions.get(token);
    }


    public Map<String, HSession> getSessions() {
        return this.sessions;
    }


}