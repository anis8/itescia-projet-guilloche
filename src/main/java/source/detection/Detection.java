package source.detection;

import io.netty.handler.codec.http.multipart.FileUpload;
import org.json.JSONObject;
import source.interfaces.detection.IDetection;
import source.utlities.uploader.Uploaded;
import source.utlities.uploader.Uploader;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public abstract class Detection implements IDetection {

    private BufferedImage buffer;
    private FileUpload file;
    private int width;
    private int height;
    private boolean result;
    private Uploaded uploaded;
    private JSONObject response;
    private String resultPath;

    public Detection() {

    }

    public Detection(FileUpload file) {
        this.result = false;
        this.response = new JSONObject();
        try {
            this.buffer = ImageIO.read(file.getFile());
            this.file = file;
            this.width = this.buffer.getWidth();
            this.height = this.buffer.getHeight();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public Color get(int col, int row) {
        return new Color(buffer.getRGB(col, row));
    }

    public void set(int col, int row, Color color) {
        buffer.setRGB(col, row, color.getRGB());
    }

    public BufferedImage getBuffer() {
        return buffer;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public FileUpload getFile() {
        return file;
    }

    public void execution() {
        this.uploaded = Uploader.saveFile(this.getFile());
        this.response.put("resultPath", this.uploaded.getPath());
    }

    public Uploaded getUploaded() {
        return uploaded;
    }

    public JSONObject getResponse() {
        return response;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String toString() {
        return "Carte d'identité française";
    }

}
