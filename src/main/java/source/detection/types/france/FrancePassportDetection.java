package source.detection.types.france;

import io.netty.handler.codec.http.multipart.FileUpload;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.objdetect.CascadeClassifier;
import source.detection.Detection;
import source.interfaces.detection.IDetection;
import source.utlities.uploader.Uploader;

public class FrancePassportDetection extends Detection implements IDetection {

    public FrancePassportDetection() {
        super();
    }

    public FrancePassportDetection(FileUpload file) {
        super(file);
    }


    @Override
    public void execution() {
        super.execution();


        this.setResult(false);
    }


    @Override
    public String toString() {
        return "Passport français";
    }
}
