package source.detection.types.france;

import io.netty.handler.codec.http.multipart.FileUpload;
import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import source.detection.Detection;
import source.interfaces.detection.IDetection;
import source.utlities.uploader.Uploader;

import java.awt.*;

public class FranceIdentityDetection extends Detection implements IDetection {

    public FranceIdentityDetection() {
        super();
    }

    public FranceIdentityDetection(FileUpload file) {
        super(file);
    }


    @Override
    public void execution() {
        super.execution();

        if (this.getUploaded() != null) {
            nu.pattern.OpenCV.loadLocally();
            Mat src = Imgcodecs.imread(Uploader.path + this.getUploaded().getPath());
            CascadeClassifier classifier = new CascadeClassifier(Uploader.path + "lbpcascade_frontalface.xml");
            MatOfRect faceDetections = new MatOfRect();
            classifier.detectMultiScale(src, faceDetections);

            int startX = 0;
            int startY = 0;
            int endX = 0;
            int endY = 0;
            for (Rect rect : faceDetections.toArray()) {
                startX = rect.x - 15;
                startY = rect.y - 30;
                endX = rect.x + rect.width + 15;
                endY = rect.y + rect.height + 30;
                Imgproc.rectangle(
                        src,
                        new Point(rect.x, rect.y),
                        new Point(rect.x + rect.width, rect.y + rect.height),
                        new Scalar(0, 0, 255),
                        3
                );
            }

            String facePath = this.getUploaded().getPath() + "-faces" + "." + this.getUploaded().getType();
            this.getResponse().put("resultPath", facePath);
            Imgcodecs.imwrite(Uploader.path + facePath, src);
            int width = this.getWidth();
            int height = this.getHeight();

            int colors = 0;
            int pixels = 0;
            for (int col = 0; col < width; col++) {
                for (int row = 0; row < height; row++) {
                    Color color = this.get(col, row);
                    int r = color.getRed();
                    int g = color.getGreen();
                    int b = color.getBlue();
                    float[] hsb = new float[3];
                    Color.RGBtoHSB(r, g, b, hsb);
                    float deg = hsb[0] * 360;
                    this.set(col, row, color);
                    if (col > startX && col < endX) {
                        if (row > startY && row < endY) {
                            pixels++;
                            if (deg >= 150 && deg < 210) {
                                colors++;
                            }
                        }
                    }
                }
            }

            Uploader.delete(this.getUploaded().getPath());

            this.setResult((colors > (pixels * 0.2)));
        }
    }

    @Override
    public String toString() {
        return "Carte d'identité française";
    }

}
