package source.detection;

import io.netty.handler.codec.http.HttpResponseStatus;
import org.json.JSONObject;
import source.interfaces.task.ITask;
import source.network.http.HttpHandler;
import source.network.http.routes.codec.RouteDecoder;
import source.network.http.sessions.HSession;

public class DetectionExecution implements ITask {

    private RouteDecoder decoder;
    private HttpResponseStatus status;
    private HSession session;
    Detection detection;

    public DetectionExecution(HSession session, RouteDecoder decoder, HttpResponseStatus status, Detection detection) {
        this.decoder = decoder;
        this.status = status;
        this.detection = detection;
        this.session = session;
    }


    @Override
    public void run() {
        this.detection.execution();
        this.detection.getResponse().put("result", detection.getResult());

        this.session.writeResponse(decoder.getCtx(), this.status, HttpHandler.TYPE_JSON, this.detection.getResponse().toString());
    }
}
