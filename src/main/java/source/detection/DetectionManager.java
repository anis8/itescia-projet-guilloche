package source.detection;

import io.netty.handler.codec.http.multipart.FileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import source.detection.types.france.FranceIdentityDetection;
import source.detection.types.france.FrancePassportDetection;
import source.interfaces.initialisable.IInitialisable;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;

public class DetectionManager implements IInitialisable {
    private static final Logger log = LogManager.getLogger(DetectionManager.class);
    private static DetectionManager detectionManager;


    private HashMap<String, Class<? extends Detection>> definitions = new HashMap<String, Class<? extends Detection>>() {{
        put("france_identity", FranceIdentityDetection.class);
        put("france_passport", FrancePassportDetection.class);
    }};

    public Detection createDetection(String type, FileUpload file) {
        Detection detection = null;
        if (this.definitions.containsKey(type)) {
            try {
                detection = this.definitions.get(type).getConstructor(FileUpload.class).newInstance(file);
            } catch (Exception e) {
                log.warn("Failed to create instance for detection: " + type, e);
            }
        }

        return detection;
    }

    public void executeDetection(DetectionExecution execution) {
        execution.run();
    }

    @Override
    public void initialise() {

    }

    public HashMap<String, String> getDetectionNames() {
        HashMap<String, String> names = new HashMap<>();
        try {
            for (String name : this.definitions.keySet()) {
                names.put(name, this.definitions.get(name).getConstructor().newInstance().toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return names;
    }

    public static DetectionManager getInstance() {
        if (detectionManager == null)
            detectionManager = new DetectionManager();

        return detectionManager;
    }
}
