/*
package source.mains;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.awt.*;
import java.io.IOException;

public class Main2 {

    public static void main(String[] args) throws IOException {

        nu.pattern.OpenCV.loadLocally();
        String file = "D:\\temp\\cartes\\nass.png";
        Mat src = Imgcodecs.imread(file);
        String xmlFile = "D:\\temp\\lbpcascade_frontalface.xml";
        CascadeClassifier classifier = new CascadeClassifier(xmlFile);
        MatOfRect faceDetections = new MatOfRect();
        classifier.detectMultiScale(src, faceDetections);

        int startX = 0;
        int startY = 0;
        int endX = 0;
        int endY = 0;

        for (Rect rect : faceDetections.toArray()) {
            startX = rect.x - 15;
            startY = rect.y - 30;
            endX = rect.x + rect.width + 15;
            endY = rect.y + rect.height + 30;

            Imgproc.rectangle(
                    src,                                               // where to draw the box
                    new Point(rect.x, rect.y),                            // bottom left
                    new Point(rect.x + rect.width, rect.y + rect.height), // top right
                    new Scalar(0, 0, 255),
                    3                                                     // RGB colour
            );


        }
        Imgcodecs.imwrite("D:\\temp\\detection.jpg", src);


        Picture picture = new Picture(file);


        int width = picture.width();
        int height = picture.height();
        Picture pictureR = new Picture(width, height);

        for (int col = 0; col < width; col++) {
            for (int row = 0; row < height; row++) {
                Color color = picture.get(col, row);
                int r = color.getRed();
                int g = color.getGreen();
                int b = color.getBlue();


                float[] hsb = new float[3];
                Color.RGBtoHSB(r, g, b, hsb);
                float deg = hsb[0] * 360;
                pictureR.set(col, row, color);
                if (col > startX && col < endX) {
                    if (row > startY && row < endY) {
                        pictureR.set(col, row, new Color(0, 0, 0));
                        if (deg >= 150 && deg < 210) {
                            pictureR.set(col, row, new Color(255, 255, 255));
                        }
                    }
                }
            }
        }

        // display  picture in its own window
        pictureR.show();


    }
}


 */