/*
package source.mains;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import source.detection.types.Picture;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        Picture picture = new Picture("D:\\temp\\noir.jpg");


        BufferedImage image = ImageIO.read(new File("D:\\temp\\carte2.jpg"));
        byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        nu.pattern.OpenCV.loadLocally();
        //put read image to Mat
        Mat mat = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3); //original Mat
        mat.put(0, 0, data);
        Mat mat_f = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3); //for storing manipulated Mat

        //conversion to grayscale, blurring and edge detection
        Imgproc.cvtColor(mat, mat_f, Imgproc.COLOR_RGB2BGR);
        Imgproc.cvtColor(mat_f, mat_f, Imgproc.COLOR_RGB2GRAY);
        Imgproc.GaussianBlur(mat_f, mat_f, new Size(13, 13), 0);
        Imgproc.Canny(mat_f, mat_f, 300, 600, 5, true);
        Imgproc.dilate(mat_f, mat_f, new Mat(), new Point(-1, -1), 2);
        Imgcodecs.imwrite("D:\\temp\\CVTest1.jpg", mat_f);

        //finding contours
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(mat_f, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        double maxArea = 0;
        int maxAreaIdx = 0;

        //finding largest contour
        for (int idx = 0; idx != contours.size(); ++idx) {
            Mat contour = contours.get(idx);
            double contourarea = Imgproc.contourArea(contour);
            if (contourarea > maxArea) {
                maxArea = contourarea;
                maxAreaIdx = idx;
            }

        }

        //Rect rect = Imgproc.boundingRect(contours.get(maxAreaIdx));
        //Imgproc.rectangle(mat, new Point(rect.x,rect.y), new Point(rect.x+rect.width,rect.y+rect.height),new Scalar(0,0,255),7);
        // mat = mat.submat(rect.y, rect.y + rect.height, rect.x, rect.x + rect.width);


        //Polygon approximation
        MatOfPoint2f approxCurve = new MatOfPoint2f();
        MatOfPoint2f oriCurve = new MatOfPoint2f(contours.get(maxAreaIdx).toArray());
        Imgproc.approxPolyDP(oriCurve, approxCurve, 6.0, true);

        //drawing red markers at vertices
        Point[] array = approxCurve.toArray();
        for (int i = 0; i < array.length; i++) {
            Imgproc.circle(mat, array[i], 2, new Scalar(0, 255, 0), 5);
        }
        Imgcodecs.imwrite("D:\\temp\\CVTest2.jpg", mat);





        int width = picture.width();
        int height = picture.height();

        // create three empy pictures of the same dimension
        Picture pictureR = new Picture(width, height);


        // separate colors
        for (int col = 0; col < width; col++) {
            for (int row = 0; row < height; row++) {
                Color color = picture.get(col, row);
                int r = color.getRed();
                int g = color.getGreen();
                int b = color.getBlue();


                pictureR.set(col, row, new Color(0, 0, 0));

                float[] hsb = new float[3];
                Color.RGBtoHSB(r, g, b, hsb);
                float deg = hsb[0] * 360;
                if (col == 100 && row == 32) {
                    System.out.println(deg);
                }
                if (deg >= 150 && deg < 210) {
                    pictureR.set(col, row, new Color(255, 255, 255));
                }
            }
        }

        // display  picture in its own window
        pictureR.show();


    }
}



 */