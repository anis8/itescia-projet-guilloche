package source.utlities;

import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.util.encoders.Hex;

import java.util.Random;
import java.util.UUID;

public class Encryption {

    public static String encryptPassword(String password, String passwordHash) {
        return encrypt(password + passwordHash);
    }

    private static String encrypt(String input) {
        SHA3.DigestSHA3 digestSHA3 = new SHA3.Digest256();
        byte[] digest = digestSHA3.digest(input.getBytes());
        return Hex.toHexString(digest);
    }

    public static String generateBigKey() {
        String hashOne = UUID.randomUUID().toString();
        String hashTwo = UUID.randomUUID().toString();
        return hashTwo + hashOne;
    }

    public static String generateString(int size) {
        String SALTCHARS = "abcdefghijklmnopqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < size) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();
    }

    public static String generateLittleKey() {
        return UUID.randomUUID().toString();
    }
}
