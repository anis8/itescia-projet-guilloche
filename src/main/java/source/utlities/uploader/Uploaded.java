package source.utlities.uploader;

public class Uploaded {

    private String path;
    private int size;
    private String type;
    private String name;

    public Uploaded() {

    }

    public Uploaded(String path, int size, String type) {
        this.path = path;
        this.size = size;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
