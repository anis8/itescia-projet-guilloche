package source.utlities.uploader;


import io.netty.handler.codec.http.multipart.FileUpload;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import source.utlities.Encryption;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.channels.FileChannel;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

public class Uploader {

    public static String path = "C:/xampp/htdocs/itescia-projet-guilloche/uploads/";
    private static String directory = null;

    public static void init() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();
        String directoryName = dtf.format(now) + "/";

        File file = new File(path + directoryName);
        if (!file.exists()) {
            file.mkdir();
        }
        directory = directoryName;
    }

    public static void delete(String pathN) {
        try {
            File file = new File(path + pathN);
            if (!file.delete()) {
                throw new Exception("Cannot delete file");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isSecureFile(String name) {
        name = fileExtension(name);
        String[] array = {"htm", "jpg", "mpg", "tgz", "xml", "zip", "svg", "wav", "7z", "xls", "sql", "torrent", "tar", "rar", "ppt", "pps", "png", "pdf", "odg", "ods", "odp", "odt", "mp4", "mp3", "mkv", "jpeg", "ico", "iso", "gz", "gif", "docx", "doc", "avi", "log", "pptx", "pem", "log", "txt"};
        List<String> list = Arrays.asList(array);
        return list.contains(name);
    }

    public static Uploaded saveFile(FileUpload file) {
        try {
            init();
            String type = fileExtension(file.getFilename());
            String pathName = Encryption.generateString(64) + "." + type;
            final File newFile = new File(path + directory + pathName);
            if (!newFile.exists()) {
                newFile.createNewFile();
            }

            FileChannel inputChannel = new FileInputStream(file.getFile()).getChannel();
            long size = inputChannel.size();
            FileChannel outputChannel = new FileOutputStream(newFile).getChannel();
            outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
            inputChannel.close();
            outputChannel.close();
            Uploaded uploaded = new Uploaded();
            uploaded.setPath(directory + pathName);
            uploaded.setSize((int) size);
            uploaded.setName(pathName);
            uploaded.setType(type);
            return uploaded;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Uploaded saveImage(BufferedImage image, String type) throws IOException {
        init();
        String pathName = Encryption.generateString(64) + "." + type;
        ImageIO.write(image, type, new File(path + directory + pathName));
        Uploaded uploaded = new Uploaded();
        uploaded.setPath(directory + pathName);
        uploaded.setSize(0);
        uploaded.setName(pathName);
        uploaded.setType(type);
        return uploaded;
    }



    public static boolean isVideo(String type) {
        String[] types = {"avi", "mpeg", "webm", "3gp", "3g2"};
        for (String s : types) {
            if (s.equals(type)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isImage(String type) {
        type = type.toLowerCase();
        String[] types = {"gif", "ico", "jpeg", "jpg", "png", "svg", "tif", "tiff", "webp"};
        for (String s : types) {
            if (s.equals(type)) {
                return true;
            }
        }
        return false;
    }

    public static String fileExtension(String url) {
        url = url.toLowerCase();
        String two = url.substring(url.length() - 2);
        String three = url.substring(url.length() - 3);
        String four = url.substring(url.length() - 4);
        if (four.contains(".")) {
            if (three.contains(".")) {
                return two;
            } else {
                return three;
            }
        } else {
            return four;
        }
    }
}
