package source.boot.properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

public class Properties extends java.util.Properties {

    private static Logger log = LogManager.getLogger(Properties.class.getName());

    public Properties(String file) {
        super();

        try {
            Reader stream = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);
            this.load(stream);
            stream.close();
        } catch (Exception e) {
            log.error("Impossible de lire le fichier de configuration:" + e);
        }
    }

    public String get(String key) {

        if(!this.containsKey(key)) {
            log.error("L'élément " + key + " n'a pas été trouvé dans le fichier de configuration");
            return null;
        }

        return this.getProperty(key);
    }

    public String get(String key, String fallback) {
        if (this.containsKey(fallback)) {
            return this.get(key);
        }

        return fallback;
    }
}