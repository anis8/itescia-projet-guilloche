package source.boot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;
import source.boot.properties.Properties;
import source.detection.DetectionManager;
import source.network.NetworkManager;
import source.task.TaskManager;

import java.io.FileInputStream;
import java.io.InputStream;


public class Rocket {

    private static Logger log;
    private static Properties config;


    public static void init() throws Exception {
        TaskManager.getInstance().initialise();
        NetworkManager.getInstance().initialise();
        DetectionManager.getInstance().initialise();
    }

    public static void run() throws Exception {
        try {
            InputStream inputStream = new FileInputStream("config/log4j2.xml");
            ConfigurationSource source = new ConfigurationSource(inputStream);
            Configurator.initialize(null, source);
        } catch (Exception e) {
            System.out.println("Error during loading Log4j2 " + e);
            return;
        }
        log = LogManager.getLogger(Rocket.class.getName());
        log.info("Rocket starting...");

        config = new Properties("config/properties.ini");
        init();
    }

    public static void exit(String message) {
        log.error("Rocket shutdown for the reason: " + message + "\"");
        System.exit(0);
    }

    public static long getTime() {
        return (System.currentTimeMillis() / 1000L);
    }

    public static Properties getConfig() {
        return config;
    }

}
