package source.interfaces.network.http;


import source.network.http.routes.codec.RouteDecoder;
import source.network.http.sessions.HSession;

public interface IRoute {
    void handle(HSession session, RouteDecoder decoded) throws Exception;

    String getContentType();
}

