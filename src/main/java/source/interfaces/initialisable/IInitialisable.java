package source.interfaces.initialisable;

import java.lang.reflect.InvocationTargetException;

public interface IInitialisable {
    void initialise() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;
}
