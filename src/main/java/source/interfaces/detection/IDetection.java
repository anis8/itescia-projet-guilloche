package source.interfaces.detection;

public interface IDetection {

    void execution();

    String toString();
}
