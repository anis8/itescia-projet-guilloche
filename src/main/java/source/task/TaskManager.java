package source.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import source.boot.Rocket;
import source.interfaces.initialisable.IInitialisable;
import source.interfaces.task.ITask;

import java.util.concurrent.*;

public class TaskManager implements IInitialisable {

    private static final Logger log = LogManager.getLogger(TaskManager.class);
    private static TaskManager taskManager;
    private ScheduledExecutorService coreExecutor;
    private int POOL_SIZE = 0;

    @Override
    public void initialise() {
        int poolSize = Integer.parseInt(Rocket.getConfig().get("task.manager.pool.size"));
        coreExecutor = Executors.newScheduledThreadPool(poolSize, r -> {
            POOL_SIZE++;
            Thread scheduledThread = new Thread(r);
            scheduledThread.setName("Rocket-Scheduler-Thread-" + POOL_SIZE);
            final Logger logThread = LogManager.getLogger("Rocket-Scheduler-Thread-" + POOL_SIZE);
            scheduledThread.setUncaughtExceptionHandler((t, e) -> logThread.error("Worker Thread Exception: ", e));
            return scheduledThread;
        });
        log.info("Ready !");
    }

    public ScheduledFuture executePeriodic(ITask task, long delay, long period, TimeUnit timeUnit) {
        return this.coreExecutor.scheduleAtFixedRate(task, delay, period, timeUnit);
    }

    public ScheduledFuture schedule(ITask task, long delay, TimeUnit timeUnit) {
        return this.coreExecutor.schedule(task, delay, timeUnit);
    }

    public void execute(ITask task) {
        this.coreExecutor.submit(task);
    }

    public static TaskManager getInstance() {
        if (taskManager == null)
            taskManager = new TaskManager();

        return taskManager;
    }
}

