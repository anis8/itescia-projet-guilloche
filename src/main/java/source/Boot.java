package source;

import source.boot.Rocket;

public class Boot {
    public static void main(String[] args) throws Exception
	{
        Rocket.run();
    }
}
